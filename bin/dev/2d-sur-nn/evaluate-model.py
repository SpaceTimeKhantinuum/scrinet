#!/usr/bin/env python

"""
code to build a 2D surrogate TD model
using Neural Networks that can be run on GPUs
"""

import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 16})

import numpy as np

import tqdm
import sys
import os
import time

from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import phenom

import lal
import lalsimulation as lalsim

from scrinet.interfaces import lalutils
from scrinet.greedy import greedyrb
from scrinet.surrogate import surrogate
from scrinet.fits import nn

import functools
import schwimmbad

import tensorflow as tf

class SinglePool(object):
    """
    from pycbc/pool.py
    used for when n_cores = 1
    """
    def broadcast(self, fcn, args):
        return self.map(fcn, [args])

    def map(self, f, items):
        return [f(a) for a in items]

def worker_wfgen(task, new_times, M, deltaT, f_min, approximant, t_min, t_max, npts):
    q, chi = task

    new_times = np.linspace(t_min, t_max, npts)

    m1, m2 = phenom.m1_m2_M_q(M, q)
    pp = dict(m1=m1, m2=m2, deltaT=deltaT,
              f_min=f_min,
              S1z=chi, S2z=chi,
              approximant=approximant
             )
    p = lalutils.gen_td_modes_wf_params(**pp)
    t, hlms = lalutils.gen_td_modes_wf(p, modes=[[2,2]])

    # convert to dimensionless units
    t = phenom.StoM(t, m1+m2)
    # get 2,2 mode as we only support this.
    h22 = hlms[(2,2)]

    mask = (t >= t_min) & (t < t_max)
    t = t[mask]
    h22 = h22[mask]

    amp = np.abs(h22)
    phase = np.unwrap(np.angle(h22))

    iamp = IUS(t, amp)
    iphase = IUS(t, phase)

    amp = iamp(new_times)
    phase = iphase(new_times)
    freq = IUS(new_times, phase).derivative()(new_times)

    tshift = lalutils.peak_align_shift(new_times, amp)
    amp = lalutils.peak_align_interp(new_times, amp, tshift)
    phase = lalutils.peak_align_interp(new_times, phase, tshift)
    freq = lalutils.peak_align_interp(new_times, freq, tshift)


    return amp, phase, freq


def gen_2d_massratio_chi_data(
    q_array,
    chi_array,
    M,
    n_cores,
    deltaT=1/1024.,
    f_min=10,
    approximant=lalsim.SEOBNRv4P,
    t_min=-10000,
    t_max=100,
    npts=5000):

    new_times = np.linspace(t_min, t_max, npts)

    worker_wfgen_partial = functools.partial(worker_wfgen,
                                             new_times=new_times,
                                             M=M,
                                             deltaT=deltaT,
                                             f_min=f_min,
                                             approximant=approximant,
                                             t_min=t_min,
                                             t_max=t_max,
                                             npts=npts)

    data = []
    coords = []

    tasks = list(zip(q_array, chi_array))

    if n_cores == 1:
        pool = SinglePool()
    else:
        pool = schwimmbad.choose_pool(mpi=False, processes=n_cores)
    results = pool.map(worker_wfgen_partial, tasks)
    try:
        pool.close
    except AttributeError:
        pass

    for i in range(len(q_array)):
        q = q_array[i]
        chi = chi_array[i]
        amp = results[i][0]
        phase = results[i][1]
        freq = results[i][2]

        d = {"t":new_times, "amp":amp, "phase":phase, "freq": freq}

        data.append(d)
        coords.append([q, chi])

    n_t_points = len(new_times)
    n_waveforms = len(data)
    ts_amp = np.zeros(shape=(n_waveforms, n_t_points))
    ts_phase = np.zeros(shape=(n_waveforms, n_t_points))
    ts_freq = np.zeros(shape=(n_waveforms, n_t_points))

    for i in range(n_waveforms):
        amp_pre_fac = phenom.eta_from_q(coords[i][0]) * lalutils.td_amp_scale(M, 1)
        ts_amp[i] = data[i]['amp'] / amp_pre_fac
        ts_phase[i] = data[i]['phase'] - data[i]['phase'][0]
        ts_freq[i] = data[i]['freq']

    return new_times, ts_amp, ts_phase, ts_freq, np.array(coords)

def build_surrogate(
    amp_or_phase,
    initial_npts=3,
    qmin=1,
    qmax=10,
    npts=1000,
    chimin=-0.99,
    chimax=0.99,
    mtotal=100,
    nvalidation_points=1000,
    greedy_tol=1e-6,
    fit_method='nn',
    scaleX=False,
    scaleY=False,
    epochs=500,
    n_cores=1,
    fit_verbose=False,
    basis_method='eim',
    nn_verbose=0,
    batch_size=None
):
    qq, cc = np.meshgrid(np.linspace(qmin, qmax, initial_npts), np.linspace(chimin, chimax, initial_npts))
    qq = qq.ravel()
    cc = cc.ravel()

    seed_x, seed_ts_amp, seed_ts_phase, seed_ts_freq, seed_ts_coords = gen_2d_massratio_chi_data(
        qq,
        cc,
        mtotal,
        n_cores=n_cores
    )
    # Make integration rule

    int_range = [seed_x[0], seed_x[-1]]
    int_num = len(seed_x)

    integration = greedyrb.Riemann(int_range, num=int_num)
    x = integration.nodes  # Define x for convenience

    sur = surrogate.Surrogate(integration, basis_method=basis_method, output_dir=amp_or_phase)

    if amp_or_phase == "amp":
        seed_ts = seed_ts_amp
    elif amp_or_phase == "phase":
        seed_ts = seed_ts_phase

    sur.build_seed_basis(ts=seed_ts, ts_coords=seed_ts_coords)

    # check seed basis and add points
    _, ts_amp, ts_phase, ts_freq, ts_coords = gen_2d_massratio_chi_data(
        np.random.uniform(qmin, qmax, npts),
        np.random.uniform(chimin, chimax, npts),
        mtotal,
        n_cores=n_cores)

    if amp_or_phase == "amp":
        ts = ts_amp
    elif amp_or_phase == "phase":
        ts = ts_phase

    sur.run_greedy_sweep(ts, ts_coords, verbose=True, greedy_tol=greedy_tol)

    # build eim
    # eim needs to be built from training_set at the greedy points
    _, ts_amp, ts_phase, _, _ = gen_2d_massratio_chi_data(
        *sur.grb.greedy_points.T,
        mtotal,
        n_cores=n_cores)
    if amp_or_phase == "amp":
        ts = ts_amp
    elif amp_or_phase == "phase":
        ts = ts_phase
    sur.build_eim(ts)

    # fit_eim
    X = sur.grb.greedy_points.copy()
    ndim = X.shape[1]
    if fit_method in ['lr', 'gpr', 'nn']:
        X[:,0] = np.log(X[:,0])

    if basis_method == 'eim':
        y = sur.grb.eim.data.T

    _, ts_amp, ts_phase, _, ext_ts_coords = gen_2d_massratio_chi_data(
        np.random.uniform(qmin, qmax, nvalidation_points),
        np.random.uniform(chimin, chimax, nvalidation_points),
        mtotal,
        n_cores=n_cores
    )

    if amp_or_phase == "amp":
        ext_ts = ts_amp
    elif amp_or_phase == "phase":
        ext_ts = ts_phase

    ext_alpha = sur.compute_projection_coefficients(ext_ts)

    Xnew = ext_ts_coords.copy()
    Xnew[:,0] = np.log(Xnew[:,0])

    Xfit = np.row_stack((X, Xnew))
    yfit = np.row_stack((y, ext_alpha))

    # compute validation points
    # check seed basis and add points
    _, vts_amp, vts_phase, _, vts_coords = gen_2d_massratio_chi_data(
        np.random.uniform(qmin, qmax, nvalidation_points),
        np.random.uniform(chimin, chimax, nvalidation_points),
        mtotal,
        n_cores=n_cores
    )

    if amp_or_phase == "amp":
        vts = vts_amp
    elif amp_or_phase == "phase":
        vts = vts_phase

    alpha_val = sur.compute_projection_coefficients(vts)

    Xval = vts_coords.copy()
    Xval[:,0] = np.log(Xval[:,0])

    validation_data=(Xval, alpha_val)

    print("fitting")
    sys.stdout.flush()
    sur.fit_eim(Xfit, yfit.T, method='nn', epochs=epochs, scaleX=scaleX, scaleY=scaleY, verbose=fit_verbose, nn_outname=amp_or_phase + "_best.h5", validation_data=validation_data, nn_verbose=nn_verbose, batch_size=batch_size, outname_prefix=amp_or_phase)

    filename = amp_or_phase + "_basis"
    print(f"saving basis to {filename}")
    sur.save_basis(filename=filename)
    sys.stdout.flush()

    filename = f"{amp_or_phase}/X_scalers"
    print(f"saving scalers to {filename}")
    sur.fits[0].save_X_scalers(filename)
    sys.stdout.flush()

    filename = f"{amp_or_phase}/Y_scalers"
    print(f"saving scalers to {filename}")
    sur.fits[0].save_Y_scalers(filename)
    sys.stdout.flush()

    # model_errors, worst_case, worst_error_index = sur.validate_surrogate(vts, vts_coords)
    # worst_error = model_errors[worst_error_index]
    # print(f"worst error = {worst_error}")
    # print(f"worst case = {worst_case}")
    # sys.stdout.flush()

    # plt.figure()
    # plt.scatter(vts_coords, model_errors)
    # plt.scatter(sur.grb.greedy_points, np.zeros(len(sur.grb.greedy_points)))
    # plt.tight_layout()
    # plt.savefig(os.path.join(sur.output_dir, f"model_errors_{amp_or_phase}.png"))
    # plt.close()

    # worst_pred = sur.predict([worst_case])

    # plt.figure()
    # plt.plot(x, worst_pred)
    # plt.plot(x, vts[worst_error_index], ls='--')
    # plt.tight_layout()
    # plt.savefig(os.path.join(sur.output_dir, f"worst_{amp_or_phase}.png"))
    # plt.close()

    # plt.figure()
    # plt.plot(x, worst_pred-vts[worst_error_index])
    # plt.tight_layout()
    # plt.savefig(os.path.join(sur.output_dir, f"worst_{amp_or_phase}_diff.png"))
    # plt.close()

    return x, sur

def wave_sur_single(q, chi, amp_sur, phase_sur):
    amp = amp_sur.predict([[q,chi]])
    phase = phase_sur.predict([[q,chi]])

    # do like this to predict multiple
    h = amp * np.exp(-1.j * phase)

    return np.real(h), np.imag(h), amp, phase

def wave_sur_many(qs, chis):
    pars = np.array(list(zip(qs, chis)))
    pars[:,0] = np.log(pars[:,0]) # need to log the mass-ratio

    amp_model, amp_basis = load_amp_model()
    amp_alpha = amp_model.predict(pars)
    amp = np.dot(amp_alpha, amp_basis)

    phase_model, phase_basis = load_phase_model()
    phase_alpha = phase_model.predict(pars)
    phase = np.dot(phase_alpha, phase_basis)

    h = amp * np.exp(-1.j * phase)

    return np.real(h), np.imag(h), amp, phase

def load_amp_model():
    model = nn.RegressionANN()
    model.load_model("amp/amp_best.h5")
    model.load_X_scalers("amp/X_scalers.npy")
    model.load_Y_scalers("amp/Y_scalers.npy")
    model.scaleX = True
    model.scaleY = True

    basis = np.load("amp/amp_basis.npy")

    return model, basis

def load_phase_model():
    model = nn.RegressionANN()
    model.load_model("phase/phase_best.h5")
    model.load_X_scalers("phase/X_scalers.npy")
    model.load_Y_scalers("phase/Y_scalers.npy")
    model.scaleX = True
    model.scaleY = True

    basis = np.load("phase/phase_basis.npy")

    return model, basis

def match(h1, h2, times):

    dt = times[1] - times[0]
    n = len(times)
    df = 1.0/(n*dt)
    norm = 4. * df

    h1_fft = np.fft.fft(h1)
    h2_fft = np.fft.fft(h2)

    h1h1_sq = np.vdot(h1_fft, h1_fft) * norm
    h2h2_sq = np.vdot(h2_fft, h2_fft) * norm

    h1h1 = dt * np.sqrt(h1h1_sq)
    h2h2 = dt * np.sqrt(h2h2_sq)


    ifft = np.fft.ifft(np.conj(h1_fft) * h2_fft)

    return ifft / h1h1 / h2h2 * 4 * dt

if __name__ == "__main__":
    print("starting...\n")
    sys.stdout.flush()


    # specify number of threads to use for tensorflow
    num_threads = 1
    #num_threads = 0
    tf.config.threading.set_inter_op_parallelism_threads(
        num_threads
    )
    print(f"tf using {tf.config.threading.get_inter_op_parallelism_threads()} thread(s)")

    n_cores=4
    mtotal=60
    qmin=1
    qmax=4
    chimin=-0.2
    chimax=0.2
    npts=5000
    nvalidation_points=100000
    epochs=5000
    batch_size=1000
    npts_match=50

    # qs = np.linspace(qmin,qmax,100)
    # chis = np.linspace(chimin,chimax,100)
    # matches = np.zeros(len(qs))
    # for i in tqdm.tqdm(range(len(qs)), file=sys.stdout):
    #     q = qs[i]
    #     chi = chis[i]
    # #     print(f"working q = {q}")
    #     vs_x, vs_amp, vs_phase, _, vt_coords = gen_2d_massratio_chi_data([q], [chi], mtotal, n_cores=1)
    #     vs_h = vs_amp[0] * np.exp(-1.j * vs_phase[0])
    #     vs_hp = np.real(vs_h)
    #     vs_hc = np.imag(vs_h)

    #     sur_hp, sur_hc, samp, sphase = wave_sur(q, chi, amp_sur=amp_sur, phase_sur=phase_sur)

    #     maxmatch = np.max(np.abs(match(vs_hp, sur_hp, vs_x)))

    #     matches[i] = maxmatch

    # plt.figure()
    # plt.scatter(range(len(matches)), 1-matches)
    # plt.yscale('log')
    # plt.ylim(1e-9)
    # plt.xlabel("q")
    # plt.ylabel("mismatch")
    # plt.tight_layout()
    # plt.savefig("matches-vs-model.png")
    # plt.close()

    qq, cc = np.meshgrid(np.linspace(qmin, qmax, npts_match), np.linspace(chimin, chimax, npts_match))
    qs = qq.ravel()
    chis = cc.ravel()
    total_number_wfs = len(qs)

    print("generating surrogate data")
    sys.stdout.flush()
    t1 = time.time()
    sur_hp, sur_hc, samp, sphase = wave_sur_many(qs, chis)
    t2 = time.time()
    dt_sur = t2-t1
    print(f"time taken (surrogate) = {dt_sur:.5f} s")
    sys.stdout.flush()

    print("generating test data")
    sys.stdout.flush()
    t1 = time.time()
    vs_x, vs_amp, vs_phase, _, vt_coords = gen_2d_massratio_chi_data(qs, chis, mtotal, n_cores=n_cores)
    t2 = time.time()
    dt_eob = t2-t1
    print(f"time taken (EOB) = {dt_eob:.5f} s")
    sys.stdout.flush()

    print("\n\n")
    print(f"time per waveform (surrogate) = {dt_sur/total_number_wfs:.5f} s")
    print(f"time per waveform (EOB) = {dt_eob/total_number_wfs*n_cores:.5f} s")
    print("\n\n")
    sys.stdout.flush()

    matches = np.zeros(len(qs))
    for i in tqdm.tqdm(range(len(qs))):
        vs_h = vs_amp[i] * np.exp(-1.j * vs_phase[i])
        vs_hp = np.real(vs_h)
        vs_hc = np.imag(vs_h)

        maxmatch = np.max(np.abs(match(vs_hp, sur_hp[i], vs_x)))

        matches[i] = maxmatch

    worst_idx = np.argmin(matches)

    print('worst match: {:.7f}'.format(np.min(matches)))
    print('best match : {:.7f}'.format(np.max(matches)))
    print('median match : {:.7f}'.format(np.median(matches)))
    print('st. dev match : {:.7f}'.format(np.std(matches)))
    sys.stdout.flush()

    print("plotting...")
    sys.stdout.flush()

    plt.figure()
    plt.scatter(qs, 1-matches)
    plt.yscale('log')
    plt.ylim(1e-9)
    plt.xlabel("q")
    plt.ylabel("mismatch")
    plt.tight_layout()
    plt.savefig("1d-q-matches-vs-model.png")
    plt.close()

    plt.figure()
    plt.scatter(chis, 1-matches)
    plt.yscale('log')
    plt.ylim(1e-9)
    plt.xlabel(r"$\chi$")
    plt.ylabel("mismatch")
    plt.tight_layout()
    plt.savefig("1d-chi-matches-vs-model.png")
    plt.close()

    plt.figure()
    plt.scatter(qs, chis, c=matches)
    plt.colorbar()
    #plt.scatter(*amp_sur.grb.greedy_points.T, marker='x', c='r', s=300)
    #plt.scatter(*phase_sur.grb.greedy_points.T, marker='x', c='k', s=300)
    plt.scatter(qs[worst_idx], chis[worst_idx], marker='o', s=100, c='k')
    plt.xlabel("q")
    plt.ylabel(r"$\chi$")
    plt.savefig("2d-matches-vs-model.png")
    plt.close()

    print("finished!")
    sys.stdout.flush()
