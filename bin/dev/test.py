import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import phenom
from scrinet.interfaces import lalutils
import lal
import lalsimulation as lalsim

import waveform1D as waveform

T800 = waveform.T800(q=1, mtotal=60, f_start=0.024, srate=0.01)

freq = T800.generate_frequency(T800.times)

plt.figure()
plt.plot(T800.times, freq)
plt.show()
plt.close()

plt.figure()
# plt.plot(T800.times, T800.amp)
plt.plot(T800.times_s, T800.amp)
plt.show()
plt.close()


# generate waveform from lal
q=1
mtotal=60
deltaT=1/1024.
f_min=10
approximant=lalsim.SEOBNRv4P
distance=1
t_min=-10000
t_max=100
npts=5000
m1, m2 = phenom.m1_m2_M_q(mtotal, q)
pp = dict(m1=m1, m2=m2, deltaT=deltaT,
            f_min=f_min,
            distance=distance,
            approximant=approximant
            )
p = lalutils.gen_td_modes_wf_params(**pp)
t, hlms = lalutils.gen_td_modes_wf(p, modes=[[2,2]])

# convert to dimensionless units
t = phenom.StoM(t, mtotal)
# get 2,2 mode as we only support this.
h22 = hlms[(2,2)]

mask = (t >= t_min) & (t < t_max)
t = t[mask]
h22 = h22[mask]

t_s = phenom.MtoS(t, mtotal)

T800 = waveform.T800(q=1, mtotal=60, times=t)

plt.figure()
plt.plot(t_s, np.abs(h22))
plt.plot(T800.times_s, T800.amp)
plt.show()
plt.close()