#!/usr/bin/env python

"""
code to build a 1D surrogate TD model
using Neural Networks that can be run on GPUs
"""

import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 16})

import numpy as np

import tqdm
import sys
import os

from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import phenom

import lal
import lalsimulation as lalsim

from scrinet.interfaces import lalutils
from scrinet.greedy import greedyrb
from scrinet.surrogate import surrogate
from scrinet.fits import nn

import tensorflow as tf

def gen_1d_massratio_data(q_array, M, deltaT=1/1024., f_min=10,
                         approximant=lalsim.SEOBNRv4P,
                          t_min=-10000, t_max=100, npts=5000
                         ):

    new_times = np.linspace(t_min, t_max, npts)

    data = []
    coords = []

    for q in q_array:
        m1, m2 = phenom.m1_m2_M_q(M, q)
        pp = dict(m1=m1, m2=m2, deltaT=deltaT,
                  f_min=f_min,
                  approximant=approximant
                 )
        p = lalutils.gen_td_modes_wf_params(**pp)
        t, hlms = lalutils.gen_td_modes_wf(p, modes=[[2,2]])

        # convert to dimensionless units
        t = phenom.StoM(t, m1+m2)
        # get 2,2 mode as we only support this.
        h22 = hlms[(2,2)]

        mask = (t >= t_min) & (t < t_max)
        t = t[mask]
        h22 = h22[mask]

        amp = np.abs(h22)
        phase = np.unwrap(np.angle(h22))

        iamp = IUS(t, amp)
        iphase = IUS(t, phase)

        amp = iamp(new_times)
        phase = iphase(new_times)
        freq = IUS(new_times, phase).derivative()(new_times)

        tshift = lalutils.peak_align_shift(new_times, amp)
        amp = lalutils.peak_align_interp(new_times, amp, tshift)
        phase = lalutils.peak_align_interp(new_times, phase, tshift)
        freq = lalutils.peak_align_interp(new_times, freq, tshift)

        d = {"t":new_times, "amp":amp, "phase":phase, "freq":freq}

        data.append(d)
        coords.append([q])

    n_t_points = len(new_times)
    n_waveforms = len(data)
    ts_amp = np.zeros(shape=(n_waveforms, n_t_points))
    ts_phase = np.zeros(shape=(n_waveforms, n_t_points))
    ts_freq = np.zeros(shape=(n_waveforms, n_t_points))

    for i in range(n_waveforms):
        amp_pre_fac = phenom.eta_from_q(coords[i][0]) * lalutils.td_amp_scale(m1+m2, p['r'])
        ts_amp[i] = data[i]['amp'] / amp_pre_fac
        ts_phase[i] = data[i]['phase'] - data[i]['phase'][0]
        ts_freq[i] = data[i]['freq']

    return new_times, ts_amp, ts_phase, ts_freq, np.array(coords)

def build_surrogate(
    amp_or_phase,
    qmin=1,
    qmax=10,
    qnpts=100,
    initial_qnum=3,
    mtotal=100,
    nvalidation_points=100,
    greedy_tol=1e-6,
    fit_method='nn',
    scaleX=False,
    scaleY=False,
    epochs=1000,
    nn_verbose=0,
    batch_size=None,
    basis_method='rb',
    fit_verbose=False
):
    """
    qnpts: initial number of points to check seed basis
    nvalidation_points: number of points to validate the model
    """
    seed_x, seed_ts_amp, seed_ts_phase, seed_ts_freq, seed_ts_coords = gen_1d_massratio_data(np.linspace(qmin, qmax, initial_qnum), mtotal)

    # Make integration rule

    int_range = [seed_x[0], seed_x[-1]]
    int_num = len(seed_x)

    integration = greedyrb.Riemann(int_range, num=int_num)
    x = integration.nodes  # Define x for convenience

    sur = surrogate.Surrogate(integration, basis_method=basis_method, output_dir=amp_or_phase)

    if amp_or_phase == "amp":
        seed_ts = seed_ts_amp
    elif amp_or_phase == "phase":
        seed_ts = seed_ts_phase
    elif amp_or_phase == "freq":
        seed_ts = seed_ts_freq

    sur.build_seed_basis(ts=seed_ts, ts_coords=seed_ts_coords)

    # check seed basis and add points
    _, ts_amp, ts_phase, ts_freq, ts_coords = gen_1d_massratio_data(np.random.uniform(qmin, qmax, qnpts), mtotal)

    if amp_or_phase == "amp":
        ts = ts_amp
    elif amp_or_phase == "phase":
        ts = ts_phase
    elif amp_or_phase == "freq":
        ts = ts_freq

    #
    sur.run_greedy_sweep(ts, ts_coords, verbose=True, greedy_tol=greedy_tol)

    # generate data at greedy points to get basis coefficients at greedy points
    _, ts_amp, ts_phase, ts_freq, _ = gen_1d_massratio_data(sur.grb.greedy_points.ravel(), mtotal)
    if amp_or_phase == "amp":
        ts = ts_amp
    elif amp_or_phase == "phase":
        ts = ts_phase
    elif amp_or_phase == "freq":
        ts = ts_freq

    if basis_method == 'eim':
        sur.build_eim(ts)

    # basis coefficients at greedy points
    alpha_ts = sur.compute_projection_coefficients(ts)

    X = sur.grb.greedy_points.copy()
    X[:,0] = np.log(X[:,0])
    y = alpha_ts

    # _, ts_amp, ts_phase, ext_ts_coords = gen_1d_massratio_data(np.random.uniform(qmin, qmax, nvalidation_points), mtotal)
    _, ts_amp, ts_phase, ts_freq, ext_ts_coords = gen_1d_massratio_data(np.linspace(qmin, qmax, nvalidation_points), mtotal)

    if amp_or_phase == "amp":
        ext_ts = ts_amp
    elif amp_or_phase == "phase":
        ext_ts = ts_phase
    elif amp_or_phase == "freq":
        ext_ts = ts_freq

    ext_alpha = sur.compute_projection_coefficients(ext_ts)

    Xnew = ext_ts_coords.copy()
    Xnew[:,0] = np.log(Xnew[:,0])

    Xfit = np.row_stack((X, Xnew))
    yfit = np.row_stack((y, ext_alpha))

    # compute validation points
    # check seed basis and add points
    _, vts_amp, vts_phase, vts_freq, vts_coords = gen_1d_massratio_data(np.random.uniform(qmin, qmax, nvalidation_points), mtotal)
    # _, vts_amp, vts_phase, vts_coords = gen_1d_massratio_data(np.linspace(qmin, qmax, 1000), mtotal)

    if amp_or_phase == "amp":
        vts = vts_amp
    elif amp_or_phase == "phase":
        vts = vts_phase
    elif amp_or_phase == "freq":
        vts = vts_freq

    # basis coefficients at validation points
    alpha_val = sur.compute_projection_coefficients(vts)
    # if basis_method == 'rb':
    #     alpha_val = sur.grb.compute_projection_coefficients_array(sur.grb.basis, vts)
    # elif basis_method == 'eim':
    #     raise NotImplementedError("eim not implemented")

    Xval = vts_coords.copy()
    Xval[:,0] = np.log(Xval[:,0])

    validation_data=(Xval, alpha_val)

    print("fitting")
    sys.stdout.flush()
    sur.fit_eim(Xfit, yfit.T, method='nn', epochs=epochs, scaleX=scaleX, scaleY=scaleY, verbose=fit_verbose, nn_outname=amp_or_phase + "_best.h5", validation_data=validation_data, nn_verbose=nn_verbose, batch_size=batch_size, outname_prefix=amp_or_phase)

    filename = amp_or_phase + "_basis"
    print(f"saving basis to {filename}")
    sur.save_basis(filename=filename)
    sys.stdout.flush()

    filename = f"{amp_or_phase}/X_scalers"
    print(f"saving scalers to {filename}")
    sur.fits[0].save_X_scalers(filename)
    sys.stdout.flush()

    filename = f"{amp_or_phase}/Y_scalers"
    print(f"saving scalers to {filename}")
    sur.fits[0].save_Y_scalers(filename)
    sys.stdout.flush()


    model_errors, worst_case, worst_error_index = sur.validate_surrogate(vts, vts_coords)
    worst_error = model_errors[worst_error_index]
    print(f"worst error = {worst_error}")
    print(f"worst case = {worst_case}")
    sys.stdout.flush()

    plt.figure()
    plt.scatter(vts_coords, model_errors)
    plt.scatter(sur.grb.greedy_points, np.zeros(len(sur.grb.greedy_points)))
    plt.tight_layout()
    plt.savefig(os.path.join(sur.output_dir, f"model_errors_{amp_or_phase}.png"))
    plt.close()

    worst_pred = sur.predict([worst_case])

    plt.figure()
    plt.plot(x, worst_pred)
    plt.plot(x, vts[worst_error_index], ls='--')
    plt.tight_layout()
    plt.savefig(os.path.join(sur.output_dir, f"worst_{amp_or_phase}.png"))
    plt.close()

    plt.figure()
    plt.plot(x, worst_pred-vts[worst_error_index])
    plt.tight_layout()
    plt.savefig(os.path.join(sur.output_dir, f"worst_{amp_or_phase}_diff.png"))
    plt.close()

    return x, sur

def wave_sur(q, amp_sur, phase_sur):

    amp = amp_sur.predict([[q]])
    phase = phase_sur.predict([[q]])
    h = amp * np.exp(-1.j * phase)

    return np.real(h), np.imag(h), amp, phase

def match(h1, h2, times):

    dt = times[1] - times[0]
    n = len(times)
    df = 1.0/(n*dt)
    norm = 4. * df

    h1_fft = np.fft.fft(h1)
    h2_fft = np.fft.fft(h2)

    h1h1_sq = np.vdot(h1_fft, h1_fft) * norm
    h2h2_sq = np.vdot(h2_fft, h2_fft) * norm

    h1h1 = dt * np.sqrt(h1h1_sq)
    h2h2 = dt * np.sqrt(h2h2_sq)


    ifft = np.fft.ifft(np.conj(h1_fft) * h2_fft)

    return ifft / h1h1 / h2h2 * 4 * dt

if __name__ == "__main__":
    print("starting...\n")
    sys.stdout.flush()


    # specify number of threads to use for tensorflow
    num_threads = 1
    tf.config.threading.set_inter_op_parallelism_threads(
        num_threads
    )
    print(f"tf using {tf.config.threading.get_inter_op_parallelism_threads()} thread(s)")

    mtotal=60
    qmin=1
    qmax=2
    basis_method = 'eim'
    # basis_method = 'rb'

    nvalidation_points = 100

    print(f"starting amp")
    sys.stdout.flush()
    amp_x, amp_sur = build_surrogate(
        amp_or_phase="amp",
        qmin=qmin,
        qmax=qmax,
        qnpts=100,
        mtotal=mtotal,
        nvalidation_points=nvalidation_points,
        greedy_tol=1e-3,
        fit_method='nn',
        scaleX=True,
        scaleY=True,
        epochs=500,
        nn_verbose=1,
        batch_size=None,
        basis_method=basis_method,
        fit_verbose=True
    )

    print(f"starting phase")
    sys.stdout.flush()
    phase_x, phase_sur = build_surrogate(
        amp_or_phase="phase",
        qmin=qmin,
        qmax=qmax,
        qnpts=100,
        mtotal=mtotal,
        nvalidation_points=nvalidation_points,
        greedy_tol=1e-6,
        fit_method='nn',
        scaleX=True,
        scaleY=True,
        epochs=500,
        nn_verbose=1,
        batch_size=None,
        basis_method=basis_method,
        fit_verbose=True
    )

    print(f"starting freq")
    sys.stdout.flush()
    freq_x, freq_sur = build_surrogate(
        amp_or_phase="freq",
        qmin=qmin,
        qmax=qmax,
        qnpts=100,
        mtotal=mtotal,
        nvalidation_points=nvalidation_points,
        greedy_tol=1e-6,
        fit_method='nn',
        scaleX=True,
        scaleY=True,
        epochs=500,
        nn_verbose=1,
        batch_size=None,
        basis_method=basis_method,
        fit_verbose=True
    )

    qs = np.linspace(qmin,qmax,100)
    matches = np.zeros(len(qs))
    for i in tqdm.tqdm(range(len(qs)), file=sys.stdout):
        q = qs[i]
    #     print(f"working q = {q}")
        vs_x, vs_amp, vs_phase, vs_freq, vt_coords = gen_1d_massratio_data([q], mtotal)
        vs_h = vs_amp[0] * np.exp(-1.j * vs_phase[0])
        vs_hp = np.real(vs_h)
        vs_hc = np.imag(vs_h)

        sur_hp, sur_hc, samp, sphase = wave_sur(q, amp_sur=amp_sur, phase_sur=phase_sur)

        maxmatch = np.max(np.abs(match(vs_hp, sur_hp, vs_x)))

        matches[i] = maxmatch

    plt.figure()
    plt.scatter(qs, 1-matches)
    plt.yscale('log')
    plt.ylim(1e-9)
    for gp in amp_sur.grb.greedy_points:
        plt.axvline(gp, c='r')
    for gp in phase_sur.grb.greedy_points:
        plt.axvline(gp, c='g', ls='--')
    plt.xlabel("q")
    plt.ylabel("mismatch")
    plt.tight_layout()
    plt.savefig("matches-vs-model.png")
    plt.close()
