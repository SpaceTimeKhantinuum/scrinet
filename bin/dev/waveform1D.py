"""
module to load a 1d surrogate waveform
and turn it into something more usable in
gravitational wave data analysis
"""

import numpy as np
import phenom
from scipy import optimize
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

from scrinet.interfaces import lalutils

import lal
import evaluate_model

class T800(object):
    def __init__(self, times=None, q=1, mtotal=1, f_start=None, srate=None, t_end=60, t_guess=-1000, distance=1):
        """
        times: times to evaluate the model at (in units of total mass)
            if None then will use f_start, srate and t_end
        q : mass-ratio
        mtotal: total mass in solar masses
        f_start: angular gw 22 geometric frequency, desired start frequency
        srate = sample rate in geometric units
        t_end = end time for times array, only if times is not explicitly given
             - geometric units
        t_guess: intial guess for root finding in geometric units, default to -1000
        distance: in meters
        """

        # 0. load ANN models
        # 1. find t_start corresponding to f_start (current 22 mode)
        # 2. build 'times' array
        # 3. compute model on 'times' array

        # 0.
        self.amp_model, self.amp_basis = evaluate_model.load_amp_model()
        self.phase_model, self.phase_basis = evaluate_model.load_phase_model()
        self.freq_model, self.freq_basis = evaluate_model.load_freq_model()

        # surrogate time grid
        self.surrogate_time_grid = np.linspace(-10000, 100, 5000)

        self.q = q
        self.mtotal = mtotal
        self.times = times
        self.f_start = f_start
        self.srate = srate
        self.t_end = t_end
        self.t_guess = t_guess
        self.distance = distance

        self.eta = phenom.eta_from_q(self.q)

        self.fin_spin = phenom.remnant.FinalSpin0815(self.eta, 0, 0)
        self.fring = phenom.remnant.fring(self.eta, 0, 0, self.fin_spin)
        # convert to angular geometric
        self.fring *= 2*np.pi

        # sanity check inputs: times not explicitly given
        if self.times is None:
            if self.srate is None:
                raise ValueError("times is None but no srate given")
            if self.f_start is None:
                raise ValueError("times is None but no srate given")
            if self.f_start > self.fring:
                raise Exception(
                        "f_start ({}) > ringdown frequency ({})".format(
                            self.f_start, self.fring))

        # sanity check inputs: times are explicitly given
        if self.times is not None:
            if self.f_start is not None:
                raise ValueError("f_start given but 'times' is not None.")
            if self.srate is not None:
                raise ValueError("srate given but 'times' is not None.")

        # setup surrogate amp and phase function for evaluation
        self.setup_amp_func()
        self.setup_phase_func()

        # determine the 'times' array to generate the model on.
        # if a 'times' array is given then use that
        # if not then generate the waveform from the given 'f_start'
        # note: this requires a root finding operation.
        self.times = self._setup_times_array(times=self.times, f_start=self.f_start, srate=self.srate, t_end=self.t_end, t_guess=self.t_guess)

        # times in seconds
        self.times_s = phenom.MtoS(self.times, self.mtotal)

        # compute amplitude and phase

        # amplitude
        self.amp = self.generate_amplitude(self.times)
        # undo eta scaling
        self.amp *= self.eta
        # put back time domain amplitude pre factor
        self.amp *= lalutils.td_amp_scale(self.mtotal, self.distance)

        # phase
        self.phase = self.generate_phase(self.times)

        # compute 22 mode
        self.h22 = self.amp * np.exp(1.j * self.phase)


    def _setup_times_array(self, times, f_start, srate, t_end, t_guess):
        """
        function to determine the times array to generate the model on.
        If an explicit times array is not given then use
        a root finding algorithm to find the time (t_start) corresponding
        to f_start and then generate a times array with a sample rate of
        1./srate from t_start until t_end.
        """
        if times is None:
            # compute times from f_start
            self.setup_freq_func()
            f_of_t_func = self._f_of_t_to_min

            try:
                self.t_start_from_f_start = self._t_of_f(f_start, f_of_t_func, t_guess=t_guess)
            except RuntimeError:
                # import matplotlib
                # import matplotlib.pyplot as plt
                # plt.figure()
                # plt.plot(self.surrogate_time_grid, self.ifreq(self.surrogate_time_grid))
                # plt.show()
                # plt.close()
                print("time of frequency root finding failed.")

                lowest_surrogate_frequency = -self.generate_frequency(self.surrogate_time_grid[0])

                if f_start < lowest_surrogate_frequency:
                    print("requested start frequency is too low")
                    print(f"lowest_surrogate_frequency = {lowest_surrogate_frequency}")
                    print("you requested f_start = {}".format(f_start))
                else:
                    print("you requested f_start = {}".format(f_start))
                    print("the ringdown frequency is = {}".format(self.fring))
                    print("possible problem:")
                    print("if you ask for an f_start too close to this then")
                    print("the root finding can fail. Try a smaller f_start")
                raise

            out_times = np.arange(self.t_start_from_f_start, t_end, srate)
        else:
            out_times = times
        return out_times

    def _f_of_t_to_min(self, t, f_start):
        """
        returns the frequency at time 't' minus 'f_start'
        using only the complete imr freq
        model of mk1.
        Used by scipy.optimize.
        """
        # note the minus sign because the freq model is negative.
        f = -self.freq_func(t)
        return (f - f_start)

    def _t_of_f(self, f_start, func, t_guess):
        """
        uses the Newton method to root find.
        Finds the time at which the frequency 'f_start' occurs.
        Used to setup initial conditions.
        Note:
            Will only work work quasi-circular, non-precessing
            and monotonic frequency functions.
        t_guess: initial guess in geometric units.
        """
        out = optimize.newton(func, t_guess, args=(f_start,))
        return out

    def setup_freq_func(self):
        """
        generate entire frequency range on surrogate time grid
        sets an attribute for the interpolating function
        Interpolate so that it can be used in the root finding.
        """
        f = self.generate_surrogate_frequency(self.q)
        t = self.surrogate_time_grid
        self.ifreq = IUS(t, f)

    def setup_amp_func(self):
        """
        generate entire amp range on surrogate time grid and
        sets an attribute for the interpolating function
        Interpolate so that it can be used in the root finding.
        """
        amp = self.generate_surrogate_amplitude(self.q)
        t = self.surrogate_time_grid
        self.iamp = IUS(t, amp)

    def setup_phase_func(self):
        """
        generate entire phase range on surrogate time grid and
        sets an attribute for the interpolating function
        Interpolate so that it can be used in the root finding.
        """
        phase = self.generate_surrogate_phase(self.q)
        t = self.surrogate_time_grid
        self.iphase = IUS(t, phase)

    def freq_func(self, t):
        """
        only used in the root finding.
        you must have self.setup_freq_func() first to use this.
        """
        return self.ifreq(t)

    def generate_surrogate_frequency(self, qs):
        """
        returns frequency on the surrogate grid
        """
        pars = np.log(qs).reshape(-1,1)
        freq_alpha = self.freq_model.predict(pars)
        freq = np.dot(freq_alpha, self.freq_basis)

        return freq

    def generate_surrogate_amplitude(self, qs):
        """
        returns amplitude on the surrogate grid
        """
        pars = np.log(qs).reshape(-1,1)
        amp_alpha = self.amp_model.predict(pars)
        amp = np.dot(amp_alpha, self.amp_basis)

        return amp

    def generate_surrogate_phase(self, qs):
        """
        returns phase on the surrogate grid
        """
        pars = np.log(qs).reshape(-1,1)
        phase_alpha = self.phase_model.predict(pars)
        phase = np.dot(phase_alpha, self.phase_basis)

        return phase

    def generate_frequency(self, t):
        """
        generate surrogate frequency on input time array
        """
        return self.ifreq(t)

    def generate_amplitude(self, t):
        """
        generate surrogate amplitude on input time array
        """
        return self.iamp(t)

    def generate_phase(self, t):
        """
        generate surrogate phase on input time array
        """
        return self.iphase(t)