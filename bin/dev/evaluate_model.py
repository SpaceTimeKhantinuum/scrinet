#!/usr/bin/env python

"""
loads models computed using fit.py
"""

import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 16})

import numpy as np

import tqdm
import sys
import os
import time

from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import phenom

import lal
import lalsimulation as lalsim

from scrinet.interfaces import lalutils
from scrinet.greedy import greedyrb
from scrinet.surrogate import surrogate
from scrinet.fits import nn

import functools
import schwimmbad

import tensorflow as tf

def gen_1d_massratio_data(q_array, M, deltaT=1/1024., f_min=10,
                         approximant=lalsim.SEOBNRv4P,
                          t_min=-10000, t_max=100, npts=5000
                         ):

    new_times = np.linspace(t_min, t_max, npts)

    data = []
    coords = []

    for q in q_array:
        m1, m2 = phenom.m1_m2_M_q(M, q)
        pp = dict(m1=m1, m2=m2, deltaT=deltaT,
                  f_min=f_min,
                  approximant=approximant
                 )
        p = lalutils.gen_td_modes_wf_params(**pp)
        t, hlms = lalutils.gen_td_modes_wf(p, modes=[[2,2]])

        # convert to dimensionless units
        t = phenom.StoM(t, m1+m2)
        # get 2,2 mode as we only support this.
        h22 = hlms[(2,2)]

        mask = (t >= t_min) & (t < t_max)
        t = t[mask]
        h22 = h22[mask]

        amp = np.abs(h22)
        phase = np.unwrap(np.angle(h22))

        iamp = IUS(t, amp)
        iphase = IUS(t, phase)

        amp = iamp(new_times)
        phase = iphase(new_times)
        freq = IUS(new_times, phase).derivative()(new_times)

        tshift = lalutils.peak_align_shift(new_times, amp)
        amp = lalutils.peak_align_interp(new_times, amp, tshift)
        phase = lalutils.peak_align_interp(new_times, phase, tshift)
        freq = lalutils.peak_align_interp(new_times, freq, tshift)

        d = {"t":new_times, "amp":amp, "phase":phase, "freq":freq}

        data.append(d)
        coords.append([q])

    n_t_points = len(new_times)
    n_waveforms = len(data)
    ts_amp = np.zeros(shape=(n_waveforms, n_t_points))
    ts_phase = np.zeros(shape=(n_waveforms, n_t_points))
    ts_freq = np.zeros(shape=(n_waveforms, n_t_points))

    for i in range(n_waveforms):
        amp_pre_fac = phenom.eta_from_q(coords[i][0]) * lalutils.td_amp_scale(m1+m2, p['r'])
        ts_amp[i] = data[i]['amp'] / amp_pre_fac
        ts_phase[i] = data[i]['phase'] - data[i]['phase'][0]
        ts_freq[i] = data[i]['freq']

    return new_times, ts_amp, ts_phase, ts_freq, np.array(coords)

def wave_sur_single(q, amp_sur, phase_sur):
    amp = amp_sur.predict([[q]])
    phase = phase_sur.predict([[q]])

    # do like this to predict multiple
    h = amp * np.exp(-1.j * phase)

    return np.real(h), np.imag(h), amp, phase

def wave_sur_many(qs):
    pars = np.log(qs).reshape(-1,1)

    amp_model, amp_basis = load_amp_model()
    amp_alpha = amp_model.predict(pars)
    amp = np.dot(amp_alpha, amp_basis)

    phase_model, phase_basis = load_phase_model()
    phase_alpha = phase_model.predict(pars)
    phase = np.dot(phase_alpha, phase_basis)

    h = amp * np.exp(-1.j * phase)

    return np.real(h), np.imag(h), amp, phase

def freq_sur_many(qs):
    pars = np.log(qs).reshape(-1,1)
    freq_model, freq_basis = load_freq_model()
    freq_alpha = freq_model.predict(pars)
    freq = np.dot(freq_alpha, freq_basis)

    return freq

def load_amp_model():
    model = nn.RegressionANN()
    model.load_model("amp/amp_best.h5")
    model.load_X_scalers("amp/X_scalers.npy")
    model.load_Y_scalers("amp/Y_scalers.npy")
    model.scaleX = True
    model.scaleY = True

    basis = np.load("amp/amp_basis.npy")

    return model, basis

def load_phase_model():
    model = nn.RegressionANN()
    model.load_model("phase/phase_best.h5")
    model.load_X_scalers("phase/X_scalers.npy")
    model.load_Y_scalers("phase/Y_scalers.npy")
    model.scaleX = True
    model.scaleY = True

    basis = np.load("phase/phase_basis.npy")

    return model, basis


def load_freq_model():
    model = nn.RegressionANN()
    model.load_model("freq/freq_best.h5")
    model.load_X_scalers("freq/X_scalers.npy")
    model.load_Y_scalers("freq/Y_scalers.npy")
    model.scaleX = True
    model.scaleY = True

    basis = np.load("freq/freq_basis.npy")

    return model, basis


def match(h1, h2, times):

    dt = times[1] - times[0]
    n = len(times)
    df = 1.0/(n*dt)
    norm = 4. * df

    h1_fft = np.fft.fft(h1)
    h2_fft = np.fft.fft(h2)

    h1h1_sq = np.vdot(h1_fft, h1_fft) * norm
    h2h2_sq = np.vdot(h2_fft, h2_fft) * norm

    h1h1 = dt * np.sqrt(h1h1_sq)
    h2h2 = dt * np.sqrt(h2h2_sq)


    ifft = np.fft.ifft(np.conj(h1_fft) * h2_fft)

    return ifft / h1h1 / h2h2 * 4 * dt

if __name__ == "__main__":
    print("starting evaluate_model\n")
    sys.stdout.flush()


    # specify number of threads to use for tensorflow
    num_threads = 1
    #num_threads = 0
    tf.config.threading.set_inter_op_parallelism_threads(
        num_threads
    )
    print(f"tf using {tf.config.threading.get_inter_op_parallelism_threads()} thread(s)")

    mtotal=60
    qmin=1
    qmax=2
    npts_match=100

    qs = np.linspace(qmin, qmax, npts_match)
    total_number_wfs = len(qs)

    print("generating surrogate data")
    sys.stdout.flush()
    t1 = time.time()
    sur_hp, sur_hc, samp, sphase = wave_sur_many(qs)
    t2 = time.time()
    dt_sur = t2-t1
    print(f"time taken (surrogate) = {dt_sur:.5f} s")
    sys.stdout.flush()

    print("generating test data")
    sys.stdout.flush()
    t1 = time.time()
    vs_x, vs_amp, vs_phase, vs_freq, vt_coords = gen_1d_massratio_data(qs, mtotal)
    t2 = time.time()
    dt_eob = t2-t1
    print(f"time taken (EOB) = {dt_eob:.5f} s")
    sys.stdout.flush()

    print("\n\n")
    print(f"time per waveform (surrogate) = {dt_sur/total_number_wfs:.5f} s")
    print(f"time per waveform (EOB) = {dt_eob/total_number_wfs:.5f} s")
    print("\n\n")
    sys.stdout.flush()

    matches = np.zeros(len(qs))
    for i in tqdm.tqdm(range(len(qs))):
        vs_h = vs_amp[i] * np.exp(-1.j * vs_phase[i])
        vs_hp = np.real(vs_h)
        vs_hc = np.imag(vs_h)

        maxmatch = np.max(np.abs(match(vs_hp, sur_hp[i], vs_x)))

        matches[i] = maxmatch

    worst_idx = np.argmin(matches)

    print('worst match: {:.7f}'.format(np.min(matches)))
    print('best match : {:.7f}'.format(np.max(matches)))
    print('median match : {:.7f}'.format(np.median(matches)))
    print('st. dev match : {:.7f}'.format(np.std(matches)))
    sys.stdout.flush()

    print("plotting...")
    sys.stdout.flush()

    plt.figure()
    plt.scatter(qs, 1-matches)
    plt.yscale('log')
    plt.ylim(1e-9)
    plt.xlabel("q")
    plt.ylabel("mismatch")
    plt.tight_layout()
    plt.savefig("1d-q-matches-vs-model.png")
    plt.close()


    print("comparing frequency")
    print("generating surrogate frequency data")
    sys.stdout.flush()
    sfreq = freq_sur_many(qs)

    print("plotting...")
    sys.stdout.flush()
    outdir="1d-freq-plots"
    os.makedirs(f"{outdir}", exist_ok=True)
    for i in tqdm.tqdm(range(len(qs[:10]))):
        fig, axes = plt.subplots(1, 3, figsize=(20, 4))
        axes[0].plot(vs_x, vs_freq[i], label='data')
        axes[0].plot(vs_x, sfreq[i], label='surrogate', ls='--')

        axes[1].plot(vs_x, vs_freq[i], label='data')
        axes[1].plot(vs_x, sfreq[i], label='surrogate', ls='--')
        axes[1].set_xlim(-300,100)

        axes[2].plot(vs_x, vs_freq[i] - sfreq[i], label='data-surrogate')
        axes[0].legend()
        axes[1].legend()
        axes[2].legend()
        plt.savefig(f"{outdir}/{i}.png")
        plt.close()




    print("finished!")
    sys.stdout.flush()
