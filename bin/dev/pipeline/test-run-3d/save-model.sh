

# run with
# bash save-model.sh


SAVEDIR=~/projects/scrinet/saved-models

MODEL_NAME=3D_NP

FULLOUT=${SAVEDIR}/${MODEL_NAME}

mkdir -p ${FULLOUT}


cp -r run/*.dag ${FULLOUT}
cp -r run/submit*.sub ${FULLOUT}


rsync -rv --relative  run/./rb/ ${FULLOUT}
rsync -rv --relative --exclude 'plots'  run/./ts/*/fits ${FULLOUT}

