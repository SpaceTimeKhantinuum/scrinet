import logging
import sys
import os
import numpy as np
import h5py

def init_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # need to do this to avoid print things twice
    # https://stackoverflow.com/a/6729713/12840171
    if not logger.handlers:
        handler = logging.StreamHandler(sys.stdout)
        handler.flush = sys.stdout.flush
        handler.setLevel(logging.INFO)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    else:
        logger.handlers[0].formatter = formatter
        logger.setLevel(logging.INFO)
        logger.handlers[0].flush()

    return logger

def load_data(data_to_model, dir_name):
    with h5py.File(os.path.join(dir_name, 'times.h5'), 'r') as f:
        x = f['times'][:]

    with h5py.File(os.path.join(dir_name, 'coords.h5'), 'r') as f:
        coords = f['data'][:]

    with h5py.File(os.path.join(dir_name, f'{data_to_model}.h5'), 'r') as f:
        data = f['data'][:]

    return x, data, coords

def load_greedy_points(filename):
    with h5py.File(filename, 'r') as f:
        coords = f['data'][:]

    return coords