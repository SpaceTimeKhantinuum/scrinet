# PARAMS

Q_MIN=4
Q_MAX=6

CHI_MIN=0.8
CHI_MAX=0.99

THETA_MIN=0
# 90 deg
THETA_MAX=1.57

OUTPUT_DIR=prec-output

BATCH_SIZE=10

INPUT_UNITS="128"
HIDDEN_UNITS="128 128 128 128 128"

LEARN_RATE=0.001

EPOCHS=500

LR_SCH=""
# LR_SCH="--lr-schedule"

USE_ALR="--use-alr"
ALR_VERBOSE=1
ALR_MIN_LR="1e-10"
ALR_PATIENCE=50
ALR_FACTOR=0.2

USE_ES="--use-es"
ES_VERBOSE=1
ES_PATIENCE=1000

KERNEL_INITIALIZER="he_uniform"
# KERNEL_INITIALIZER="he_normal"
# KERNEL_INITIALIZER="glorot_uniform"

# ACTIVATION='relu'
ACTIVATION='elu'
# ACTIVATION='tanh'

# gen wf data
#./gen_wf_data_3d_prec_single_spin.py --grid regular --npts 4 -v --n-cores 20 --output-dir ${OUTPUT_DIR}/seed_wf_data --q-min ${Q_MIN} --q-max ${Q_MAX} --chi1-min ${CHI_MIN} --chi1-max ${CHI_MAX} --theta1-min ${THETA_MIN} --theta1-max ${THETA_MAX}
#./gen_wf_data_3d_prec_single_spin.py --grid random --npts 10000 -v --n-cores 30 --output-dir ${OUTPUT_DIR}/train_wf_data --q-min ${Q_MIN} --q-max ${Q_MAX} --chi1-min ${CHI_MIN} --chi1-max ${CHI_MAX} --theta1-min ${THETA_MIN} --theta1-max ${THETA_MAX}
#./gen_wf_data_3d_prec_single_spin.py --grid random --npts 100 -v --n-cores 30 --output-dir ${OUTPUT_DIR}/validation_wf_data --q-min ${Q_MIN} --q-max ${Q_MAX} --chi1-min ${CHI_MIN} --chi1-max ${CHI_MAX} --theta1-min ${THETA_MIN} --theta1-max ${THETA_MAX}
#./gen_wf_data_3d_prec_single_spin.py --grid random --npts 100 -v --n-cores 30 --output-dir ${OUTPUT_DIR}/test_wf_data --q-min ${Q_MIN} --q-max ${Q_MAX} --chi1-min ${CHI_MIN} --chi1-max ${CHI_MAX} --theta1-min ${THETA_MIN} --theta1-max ${THETA_MAX}

# build reduced basis
./build_rb.py --data-to-model amp -v --greedy-tol 1e-4 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data
./build_rb.py --data-to-model phase -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data

# training data to fit
./gen_ts_data.py --train-or-val train --data-to-model amp -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb
./gen_ts_data.py --train-or-val train --data-to-model phase -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb

# validation data to test fit
./gen_ts_data.py --train-or-val val --data-to-model amp -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb
./gen_ts_data.py --train-or-val val --data-to-model phase -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb

# fit
#./fit.py --plot-fits --data-to-model amp -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}
#./fit.py --plot-fits --data-to-model phase -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

# evaluate
#./evaluate_model.py -v --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy  --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/test_wf_data
#./evaluate_model.py -v --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy  --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/train_wf_data
#./evaluate_model.py -v --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy  --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/validation_wf_data
