
# get wf data
./gen_wf_data.py --grid regular --npts 2 -v --n-cores 4 --output-dir seed_wf_data --q-max 2 --chi-min -0.2 --chi-max 0.2
./gen_wf_data.py --grid random --npts 100 -v --n-cores 4 --output-dir train_wf_data --q-max 2  --chi-min -0.2 --chi-max 0.2
./gen_wf_data.py --grid random --npts 200 -v --n-cores 4 --output-dir validation_wf_data --q-max 2  --chi-min -0.2 --chi-max 0.2
./gen_wf_data.py --grid regular --npts 20 -v --n-cores 4 --output-dir test_wf_data --q-max 2  --chi-min -0.2 --chi-max 0.2

# build rb
./build_rb.py --data-to-model amp -v --greedy-tol 1e-6
./build_rb.py --data-to-model phase -v --greedy-tol 1e-8

# training data to fit
./gen_ts_data.py --train-or-val train --data-to-model amp -v --basis-method eim --wf-dir train_wf_data --basis-dir rb
./gen_ts_data.py --train-or-val train --data-to-model phase -v --basis-method eim --wf-dir train_wf_data --basis-dir rb

# validation data to test fit
./gen_ts_data.py --train-or-val val --data-to-model amp -v --basis-method eim --wf-dir validation_wf_data --basis-dir rb
./gen_ts_data.py --train-or-val val --data-to-model phase -v --basis-method eim --wf-dir validation_wf_data --basis-dir rb

# fit
./fit.py --data-to-model amp -v --scaleX --scaleY --epochs 400  --batch-size 1000
./fit.py --data-to-model phase -v --scaleX --scaleY --epochs 800  --batch-size 1000

# evaluate
./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir test_wf_data
./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir train_wf_data
./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir validation_wf_data
