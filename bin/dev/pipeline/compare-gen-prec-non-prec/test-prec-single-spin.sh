# PARAMS

Q_MIN=4
Q_MAX=6

OUTPUT_DIR=prec-output

# gen wf data
../gen_wf_data_3d_prec_single_spin.py --grid random --npts 100 -v --n-cores 4 --output-dir ${OUTPUT_DIR}/train_wf_data --q-min ${Q_MIN} --q-max ${Q_MAX}


OUTPUT_DIR=non-prec-output

../gen_wf_data_3d_non_prec.py --grid random --npts 100 -v --n-cores 4 --output-dir ${OUTPUT_DIR}/train_wf_data --q-min ${Q_MIN} --q-max ${Q_MAX}