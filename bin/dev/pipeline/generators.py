"""
module that contains helper functions to generate waveforms
"""

import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import phenom
import lal
import lalsimulation as lalsim
from scrinet.interfaces import lalutils
import functools
import schwimmbad

class SinglePool(object):
    """
    from pycbc/pool.py
    used for when n_cores = 1
    """
    def broadcast(self, fcn, args):
        return self.map(fcn, [args])

    def map(self, f, items):
        return [f(a) for a in items]

def cart_to_polar(x, y, z):
    """
    cartesian to spherical polar transformation
    returns: r, theta, phi
    """
    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arccos(z / r)
    phi = np.arctan2(y, x)
    return r, theta, phi

def polar_to_cart(r, theta, phi):
    """
    spherical polar to cartesian transformation
    returns: x, y, z
    """
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    return x, y, z

#### 2d non-precessing, equal spin on both BHs

def worker_wfgen_2d(task, new_times, M, deltaT, f_min, approximant, t_min, t_max, npts):
    q, chi = task

    new_times = np.linspace(t_min, t_max, npts)

    m1, m2 = phenom.m1_m2_M_q(M, q)
    pp = dict(m1=m1, m2=m2, deltaT=deltaT,
              f_min=f_min,
              S1z=chi, S2z=chi,
              approximant=approximant
             )
    p = lalutils.gen_td_modes_wf_params(**pp)
    t, hlms = lalutils.gen_td_modes_wf(p, modes=[[2,2]])

    # convert to dimensionless units
    t = phenom.StoM(t, m1+m2)
    # get 2,2 mode as we only support this.
    h22 = hlms[(2,2)]

    if t_min < t[0]:
        print(f"possible interpolation issue: t_min ({t_min}) < t[0] ({t[0]:.2f})", flush=True)

    mask = (t >= t_min) & (t < t_max)
    t = t[mask]
    h22 = h22[mask]

    amp = np.abs(h22)
    phase = np.unwrap(np.angle(h22))

    iamp = IUS(t, amp)
    iphase = IUS(t, phase)

    amp = iamp(new_times)
    phase = iphase(new_times)
    freq = IUS(new_times, phase).derivative()(new_times)

    tshift = lalutils.peak_align_shift(new_times, amp)
    amp = lalutils.peak_align_interp(new_times, amp, tshift)
    phase = lalutils.peak_align_interp(new_times, phase, tshift)
    freq = lalutils.peak_align_interp(new_times, freq, tshift)


    return amp, phase, freq


def gen_2d_massratio_chi_data(
    q_array,
    chi_array,
    M,
    n_cores,
    deltaT=1/1024.,
    f_min=10,
    approximant=lalsim.SEOBNRv4P,
    t_min=-10000,
    t_max=100,
    npts=5000):

    new_times = np.linspace(t_min, t_max, npts)

    worker_wfgen_partial = functools.partial(worker_wfgen_2d,
                                             new_times=new_times,
                                             M=M,
                                             deltaT=deltaT,
                                             f_min=f_min,
                                             approximant=approximant,
                                             t_min=t_min,
                                             t_max=t_max,
                                             npts=npts)

    data = []
    coords = []

    tasks = list(zip(q_array, chi_array))

    if n_cores == 1:
        pool = SinglePool()
    else:
        pool = schwimmbad.choose_pool(mpi=False, processes=n_cores)
    results = pool.map(worker_wfgen_partial, tasks)
    try:
        pool.close
    except AttributeError:
        pass

    for i in range(len(q_array)):
        q = q_array[i]
        chi = chi_array[i]
        amp = results[i][0]
        phase = results[i][1]
        freq = results[i][2]

        d = {"t":new_times, "amp":amp, "phase":phase, "freq": freq}

        data.append(d)
        coords.append([q, chi])

    n_t_points = len(new_times)
    n_waveforms = len(data)
    ts_amp = np.zeros(shape=(n_waveforms, n_t_points))
    ts_phase = np.zeros(shape=(n_waveforms, n_t_points))
    ts_freq = np.zeros(shape=(n_waveforms, n_t_points))

    for i in range(n_waveforms):
        amp_pre_fac = phenom.eta_from_q(coords[i][0]) * lalutils.td_amp_scale(M, 1)
        ts_amp[i] = data[i]['amp'] / amp_pre_fac
        ts_phase[i] = data[i]['phase'] - data[i]['phase'][0]
        ts_freq[i] = data[i]['freq']

    return new_times, ts_amp, ts_phase, ts_freq, np.array(coords)


### 3d non-precessing

def worker_wfgen_3d_non_prec(task, new_times, M, deltaT, f_min, approximant, t_min, t_max, npts):
    q, chi1z, chi2z = task

    new_times = np.linspace(t_min, t_max, npts)

    m1, m2 = phenom.m1_m2_M_q(M, q)
    pp = dict(m1=m1, m2=m2, deltaT=deltaT,
              f_min=f_min,
              S1z=chi1z, S2z=chi2z,
              approximant=approximant
             )
    p = lalutils.gen_td_modes_wf_params(**pp)
    t, hlms = lalutils.gen_td_modes_wf(p, modes=[[2,2]])

    # convert to dimensionless units
    t = phenom.StoM(t, m1+m2)
    # get 2,2 mode as we only support this.
    h22 = hlms[(2,2)]

    if t_min < t[0]:
        print(f"possible interpolation issue: t_min ({t_min}) < t[0] ({t[0]:.2f})", flush=True)

    mask = (t >= t_min) & (t < t_max)
    t = t[mask]
    h22 = h22[mask]

    amp = np.abs(h22)
    phase = np.unwrap(np.angle(h22))

    iamp = IUS(t, amp)
    iphase = IUS(t, phase)

    amp = iamp(new_times)
    phase = iphase(new_times)
    freq = IUS(new_times, phase).derivative()(new_times)

    tshift = lalutils.peak_align_shift(new_times, amp)
    amp = lalutils.peak_align_interp(new_times, amp, tshift)
    phase = lalutils.peak_align_interp(new_times, phase, tshift)
    freq = lalutils.peak_align_interp(new_times, freq, tshift)


    return amp, phase, freq


def gen_3d_non_prec_data(
    q_array,
    chi1z_array,
    chi2z_array,
    M,
    n_cores,
    deltaT=1/1024.,
    f_min=10,
    approximant=lalsim.SEOBNRv4P,
    t_min=-10000,
    t_max=100,
    npts=5000):

    new_times = np.linspace(t_min, t_max, npts)

    worker_wfgen_partial = functools.partial(worker_wfgen_3d_non_prec,
                                             new_times=new_times,
                                             M=M,
                                             deltaT=deltaT,
                                             f_min=f_min,
                                             approximant=approximant,
                                             t_min=t_min,
                                             t_max=t_max,
                                             npts=npts)

    data = []
    coords = []

    tasks = list(zip(q_array, chi1z_array, chi2z_array))

    if n_cores == 1:
        pool = SinglePool()
    else:
        pool = schwimmbad.choose_pool(mpi=False, processes=n_cores)
    results = pool.map(worker_wfgen_partial, tasks)
    try:
        pool.close
    except AttributeError:
        pass

    for i in range(len(q_array)):
        q = q_array[i]
        chi1z = chi1z_array[i]
        chi2z = chi2z_array[i]
        amp = results[i][0]
        phase = results[i][1]
        freq = results[i][2]

        d = {"t":new_times, "amp":amp, "phase":phase, "freq": freq}

        data.append(d)
        coords.append([q, chi1z, chi2z])

    n_t_points = len(new_times)
    n_waveforms = len(data)
    ts_amp = np.zeros(shape=(n_waveforms, n_t_points))
    ts_phase = np.zeros(shape=(n_waveforms, n_t_points))
    ts_freq = np.zeros(shape=(n_waveforms, n_t_points))

    for i in range(n_waveforms):
        amp_pre_fac = phenom.eta_from_q(coords[i][0]) * lalutils.td_amp_scale(M, 1)
        ts_amp[i] = data[i]['amp'] / amp_pre_fac
        ts_phase[i] = data[i]['phase'] - data[i]['phase'][0]
        ts_freq[i] = data[i]['freq']

    return new_times, ts_amp, ts_phase, ts_freq, np.array(coords)











### 3d precession single spin

def worker_wfgen_3d_prec_single_spin(task, new_times, M, deltaT, f_min, approximant, t_min, t_max, npts, modes):
    q, chi1x, chi1z = task

    new_times = np.linspace(t_min, t_max, npts)

    m1, m2 = phenom.m1_m2_M_q(M, q)
    pp = dict(m1=m1, m2=m2, deltaT=deltaT,
              f_min=f_min,
              S1x=chi1x, S1z=chi1z,
              approximant=approximant
             )
    p = lalutils.gen_td_modes_wf_params(**pp)

    t, hlms = lalutils.gen_td_modes_wf(p, modes=modes)

    # convert to dimensionless units
    t = phenom.StoM(t, m1+m2)

    # compute peak time from 2,2 mode for now - this is a simplification
    # that should be changed later

    h22 = hlms[(2,2)]

    if t_min < t[0]:
        print(f"possible interpolation issue: t_min ({t_min}) < t[0] ({t[0]:.2f})", flush=True)

    mask = (t >= t_min) & (t < t_max)
    t = t[mask]
    h22 = h22[mask]

    amp = np.abs(h22)
    phase = np.unwrap(np.angle(h22))

    iamp = IUS(t, amp)
    iphase = IUS(t, phase)

    amp = iamp(new_times)
    phase = iphase(new_times)
    freq = IUS(new_times, phase).derivative()(new_times)

    tshift = lalutils.peak_align_shift(new_times, amp)
    amp = lalutils.peak_align_interp(new_times, amp, tshift)
    phase = lalutils.peak_align_interp(new_times, phase, tshift)
    freq = lalutils.peak_align_interp(new_times, freq, tshift)

    return amp, phase, freq

def gen_3d_prec_single_spin_data(
    q_array,
    chi1_array,
    theta1_array,
    M,
    n_cores,
    deltaT=1/1024.,
    f_min=10,
    approximant=lalsim.SEOBNRv4P,
    t_min=-10000,
    t_max=100,
    npts=5000):
    """
    parameters:
        mass-ratio
        spin1 magnitude
        theta1 (polar angle of spin)
    """
    # convert polar to cartesian
    phi1_array = np.zeros(len(chi1_array))
    chi1x_array, chi1y_array, chi1z_array = polar_to_cart(chi1_array, theta1_array, phi1_array)

    new_times = np.linspace(t_min, t_max, npts)

    # just modelling 2,2 interial frame mode for now to see what it looks like
    # default_modes = [[2,2], [2,1], [2,0], [2,-1], [2,-2]]
    default_modes = [[2,2]]

    worker_wfgen_partial = functools.partial(worker_wfgen_3d_prec_single_spin,
                                             new_times=new_times,
                                             M=M,
                                             deltaT=deltaT,
                                             f_min=f_min,
                                             approximant=approximant,
                                             t_min=t_min,
                                             t_max=t_max,
                                             npts=npts,
                                             modes=default_modes)

    data = []
    coords = []

    tasks = list(zip(q_array, chi1x_array, chi1z_array))

    if n_cores == 1:
        pool = SinglePool()
    else:
        pool = schwimmbad.choose_pool(mpi=False, processes=n_cores)
    results = pool.map(worker_wfgen_partial, tasks)
    try:
        pool.close
    except AttributeError:
        pass

    for i in range(len(q_array)):
        q = q_array[i]
        chi1x = chi1x_array[i]
        chi1z = chi1z_array[i]
        amp = results[i][0]
        phase = results[i][1]
        freq = results[i][2]

        d = {"t":new_times, "amp":amp, "phase":phase, "freq": freq}

        data.append(d)
        coords.append([q, chi1x, chi1z])

    n_t_points = len(new_times)
    n_waveforms = len(data)
    ts_amp = np.zeros(shape=(n_waveforms, n_t_points))
    ts_phase = np.zeros(shape=(n_waveforms, n_t_points))
    ts_freq = np.zeros(shape=(n_waveforms, n_t_points))

    for i in range(n_waveforms):
        amp_pre_fac = phenom.eta_from_q(coords[i][0]) * lalutils.td_amp_scale(M, 1)
        ts_amp[i] = data[i]['amp'] / amp_pre_fac
        ts_phase[i] = data[i]['phase'] - data[i]['phase'][0]
        ts_freq[i] = data[i]['freq']

    return new_times, ts_amp, ts_phase, ts_freq, np.array(coords)