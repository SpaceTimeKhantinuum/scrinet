
# fit
#./fit.py --data-to-model amp -v --scaleX --scaleY --epochs 500  --batch-size 1000 --input-units 512 --hidden-units 512 512
#./fit.py --data-to-model phase -v --scaleX --scaleY --epochs 1000  --batch-size 1000 --input-units 512 --hidden-units 512 512

# evaluate
./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir train_wf_data
./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir validation_wf_data
./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir test_wf_data
