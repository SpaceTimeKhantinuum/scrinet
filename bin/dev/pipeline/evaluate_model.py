#!/usr/bin/env python

"""
loads models computed using fit.py

example

./evaluate_model.py -v --amp-basis rb/amp/amp_eim_basis.npy  --amp-model-dir ts/amp/fits --phase-basis rb/phase/phase_eim_basis.npy  --phase-model-dir ts/phase/fits --wf-dir train_wf_data
"""

import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 16})

import os
import numpy as np
import argparse
import tqdm
import time
import pathlib

import tensorflow as tf


from scrinet.fits.nn import RegressionANN

from pipe_utils import init_logger, load_data

def load_model(basis_file, nn_weights_file, X_scalers_file, Y_scalers_file):
    model = RegressionANN()
    model.load_model(nn_weights_file)
    model.load_X_scalers(X_scalers_file)
    model.load_Y_scalers(Y_scalers_file)
    model.scaleX = True
    model.scaleY = True

    basis = np.load(basis_file)

    return model, basis

# def wave_sur_many(qs, chi1xs, chi1zs, amp_model, amp_basis, phase_model, phase_basis):
# def wave_sur_many(qs, chis, amp_model, amp_basis, phase_model, phase_basis):
def wave_sur_many(pars, amp_model, amp_basis, phase_model, phase_basis):
    """
    pars: np.ndarray of pars to evaluate model
        pars.shape = (N,M) where N is the number of waveforms
            and M is the dimensionality.
            M=0 should always be mass-ratio
    """
    # pars = np.array(list(zip(qs, chis)))
    # pars = np.array(list(zip(qs, chi1xs, chi1zs)))
    pars = pars.copy()
    pars[:,0] = np.log(pars[:,0]) # need to log the mass-ratio

    amp_alpha = amp_model.predict(pars)
    amp = np.dot(amp_alpha, amp_basis)

    phase_alpha = phase_model.predict(pars)
    phase = np.dot(phase_alpha, phase_basis)

    h = amp * np.exp(-1.j * phase)

    return np.real(h), np.imag(h), amp, phase

def match(h1, h2, times):

    dt = times[1] - times[0]
    n = len(times)
    df = 1.0/(n*dt)
    norm = 4. * df

    h1_fft = np.fft.fft(h1)
    h2_fft = np.fft.fft(h2)

    h1h1_sq = np.vdot(h1_fft, h1_fft) * norm
    h2h2_sq = np.vdot(h2_fft, h2_fft) * norm

    h1h1 = dt * np.sqrt(h1h1_sq)
    h2h2 = dt * np.sqrt(h2h2_sq)


    ifft = np.fft.ifft(np.conj(h1_fft) * h2_fft)

    return ifft / h1h1 / h2h2 * 4 * dt

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--output-dir", type=str, default='evaluate',
                        help="directory to save data")
    parser.add_argument("--wf-dir", type=str, required=True,
                        help="directory of waveform data")

    parser.add_argument("--amp-basis", type=str, required=True,
                        help="path to amp basis")
    parser.add_argument("--amp-model-dir", type=str, required=True,
                        help="dir of amp model weights and scalers")
    parser.add_argument("--phase-basis", type=str, required=True,
                        help="path to phase basis")
    parser.add_argument("--phase-model-dir", type=str, required=True,
                        help="dir of phase model weights and scalers")


    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()

    logger = init_logger()

    if args.verbose:
        logger.info(f"current file: {__file__}")
        logger.info("verbosity turned on")
        logger.info(f"amp basis: {args.amp_basis}")
        logger.info(f"amp model dir: {args.amp_model_dir}")
        logger.info(f"phase basis: {args.phase_basis}")
        logger.info(f"phase model dir: {args.phase_model_dir}")

    sub_dir = pathlib.PurePath(args.wf_dir).name
    output_data_dir = os.path.join(args.output_dir, sub_dir)

    if args.verbose:
        logger.info("making outdir tree")
        logger.info(f"making dir: {output_data_dir}")
    os.makedirs(f"{output_data_dir}", exist_ok=True)

    # specify number of threads to use for tensorflow
    num_threads = 1
    #num_threads = 0
    tf.config.threading.set_inter_op_parallelism_threads(num_threads)
    tf_current_threads = tf.config.threading.get_inter_op_parallelism_threads()
    if args.verbose:
        logger.info(f"tf using {tf_current_threads} thread(s)")


    if args.verbose:
        logger.info(f"loading amp model")

    amp_model, amp_basis = load_model(
        basis_file=args.amp_basis,
        nn_weights_file=os.path.join(args.amp_model_dir, "best.h5"),
        X_scalers_file=os.path.join(args.amp_model_dir, "X_scalers.npy"),
        Y_scalers_file=os.path.join(args.amp_model_dir, "Y_scalers.npy"))

    if args.verbose:
        logger.info(f"loading phase model")
    phase_model, phase_basis = load_model(
        basis_file=args.phase_basis,
        nn_weights_file=os.path.join(args.phase_model_dir, "best.h5"),
        X_scalers_file=os.path.join(args.phase_model_dir, "X_scalers.npy"),
        Y_scalers_file=os.path.join(args.phase_model_dir, "Y_scalers.npy"))

    if args.verbose:
        logger.info(f"loading wf data from {args.wf_dir}")
    wf_x, wf_ts_amp, wf_ts_coords = load_data(
        data_to_model="amp", dir_name=args.wf_dir)
    _, wf_ts_phase, _ = load_data(
        data_to_model="phase", dir_name=args.wf_dir)

    wf_ts_h22 = np.zeros(shape=wf_ts_amp.shape, dtype=np.complex128)
    for i in range(wf_ts_h22.shape[0]):
        vs_amp = wf_ts_amp[i]
        vs_phase = wf_ts_phase[i]
        wf_ts_h22[i] = vs_amp * np.exp(-1.j*vs_phase)

    qs = wf_ts_coords[:,0]
    chis = wf_ts_coords[:,1]
    # chi1xs = wf_ts_coords[:,1]
    # chi1zs = wf_ts_coords[:,2]
    total_number_wfs = len(qs)

    if args.verbose:
        logger.info(f"total number of waveforms: {total_number_wfs}")
        logger.info("generating surrogate data")
    t1 = time.time()
    sur_hp, sur_hc, samp, sphase = wave_sur_many(wf_ts_coords, amp_model, amp_basis, phase_model, phase_basis)
    # sur_hp, sur_hc, samp, sphase = wave_sur_many(qs, chis, amp_model, amp_basis, phase_model, phase_basis)
    # sur_hp, sur_hc, samp, sphase = wave_sur_many(qs, chi1xs, chi1zs, amp_model, amp_basis, phase_model, phase_basis)
    t2 = time.time()
    dt_sur = t2-t1

    if args.verbose:
        logger.info(f"time taken (surrogate) = {dt_sur:.5f} s")
        logger.info(f"time per waveform (surrogate) = {dt_sur/total_number_wfs:.5f} s")

    if args.verbose:
        logger.info("computing matches")
    matches = np.zeros(len(qs))
    for i in tqdm.tqdm(range(len(qs))):
        vs_h = wf_ts_h22[i]
        vs_hp = np.real(vs_h)
        # vs_hc = np.imag(vs_h)

        maxmatch = np.max(np.abs(match(vs_hp, sur_hp[i], wf_x)))

        matches[i] = maxmatch

    worst_idx = np.argmin(matches)

    if args.verbose:
        logger.info(f"worst match: {np.min(matches):.7f}")
        logger.info(f"best match : {np.max(matches):.7f}")
        logger.info(f"median match : {np.median(matches):.7f}")
        logger.info(f"st. dev match : {np.std(matches):.7f}")

    if args.verbose:
        best_idx = np.argmax(matches)
        best_coords = wf_ts_coords[best_idx]
        logger.info(f"best coords: {best_coords}")

    if args.verbose:
        logger.info("plotting...")


    plt.figure()
    plt.scatter(range(len(matches)), 1-matches)
    plt.yscale('log')
    plt.ylim(1e-9)
    plt.xlabel("case")
    plt.ylabel("mismatch")
    plt.tight_layout()
    plt.savefig(os.path.join(output_data_dir, "1d-matches-vs-model.png"))
    plt.close()

    if wf_ts_coords.shape[1] == 2:

        plt.figure()
        plt.scatter(qs, 1-matches)
        plt.yscale('log')
        plt.ylim(1e-9)
        plt.xlabel("q")
        plt.ylabel("mismatch")
        plt.tight_layout()
        plt.savefig(os.path.join(output_data_dir, "1d-q-matches-vs-model.png"))
        plt.close()

        plt.figure()
        plt.scatter(chis, 1-matches)
        plt.yscale('log')
        plt.ylim(1e-9)
        plt.xlabel(r"$\chi$")
        plt.ylabel("mismatch")
        plt.tight_layout()
        plt.savefig(os.path.join(output_data_dir, "1d-chi-matches-vs-model.png"))
        plt.close()

        plt.figure()
        plt.scatter(qs, chis, c=matches)
        plt.colorbar()
        plt.scatter(qs[worst_idx], chis[worst_idx], marker='o', s=100, c='k')
        plt.xlabel("q")
        plt.ylabel(r"$\chi$")
        plt.tight_layout()
        plt.savefig(os.path.join(output_data_dir, "2d-matches-vs-model.png"))
        plt.close()

    if args.verbose:
        logger.info("finished!")
