# example how to use scatter histogram
# I took the code to generate scatter histograms from
# from gwastro/pycbc
# https://github.com/gwastro/pycbc/blob/master/pycbc/results/scatter_histograms.py

import numpy as np

from scrinet.results.results import create_multidim_plot

mean = (1, 2)
cov = [[1, 0], [0, 100]]
x, y = np.random.multivariate_normal(mean, cov, (1000, 1000)).T

parameters = ['x0', 'x1', 'y0', 'y1']

samples = np.recarray(len(x), dtype=[(p, float) for p in parameters])
samples['x0'] = x[:,0]
samples['x1'] = x[:,1]
samples['y0'] = y[:,0]
samples['y1'] = y[:,1]

# some fake zvals
zvals = x[:,0]

pars_to_show = ['x0', 'x1', 'y0']
labels = {'x0':r'$x_0$', 'x1':r'$x_1$', 'y0':r'$y_0$'}
# mins = {'x0':-4, 'x1':-4, 'y0':-4}
# maxs = {'x0':4, 'x1':4, 'y0':4}

fig, axes_dict = create_multidim_plot(
    pars_to_show,
    samples,
    zvals=zvals,
    show_colorbar=True,
    labels=labels,
    cbar_label='Zvals',
    cb_scale=14)
fig.suptitle('Title')
fig.savefig("example.png")