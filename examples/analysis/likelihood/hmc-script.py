import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use('agg')

mpl.rcParams.update({"font.size": 16})


import numpy as np
# import seaborn as sns
import pandas as pd
import arviz as az

import tensorflow as tf
import tensorflow_probability as tfp

from tensorflow import keras
from tensorflow.keras import layers

from sklearn.model_selection import train_test_split

# import tensorflow_docs as tfdocs
# import tensorflow_docs.plots
# import tensorflow_docs.modeling

import scrinet.sample.sample_helpers as nn_help
import scrinet.sample.likelihood as likelihood
import scrinet.sample.kernels as kernel_helper


from scrinet.greedy import greedyrb
from scrinet.surrogate import surrogate
from scrinet.fits import nn, scale
from scrinet.interfaces import lalutils
from scrinet.analysis import utils as ut

from scrinet.analysis.timeseries_batch import TimeSeries
from scrinet.analysis.frequencyseries_batch import FrequencySeries
from scrinet.analysis import matchedfilter_batch as matchedfilter

from scrinet.workflow.pipe_utils import (
    init_logger,
    load_data,
    match,
    wave_sur_many,
    load_model,
)

from scrinet.workflow import generators
import schwimmbad
import functools
import tqdm
import os
import scrinet
import time

import argparse
import subprocess
import datetime

tfb = tfp.bijectors
tfd = tfp.distributions

def check_gpu():
    logger.info("running 'tf.config.list_physical_devices('GPU')'")
    logger.info(tf.config.list_physical_devices("GPU"))

    try:
        logger.info("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        logger.info("could not run 'nvidia-smi -L'")

def load_annsur(rootdir):
    amp_basis = os.path.join(rootdir, "rb/amp/amp_eim_basis.npy")
    amp_model_dir = os.path.join(rootdir, "ts/amp/fits")
    
    phase_basis = os.path.join(rootdir, "rb/phase/phase_eim_basis.npy")
    phase_model_dir = os.path.join(rootdir, "ts/phase/fits")
    amp_model, amp_basis = load_model(
        basis_file=amp_basis,
        nn_weights_file=os.path.join(amp_model_dir, "best.h5"),
        X_scalers_file=os.path.join(amp_model_dir, "X_scalers.npy"),
        Y_scalers_file=os.path.join(amp_model_dir, "Y_scalers.npy"),
    )
    
    phase_model, phase_basis = load_model(
        basis_file=phase_basis,
        nn_weights_file=os.path.join(phase_model_dir, "best.h5"),
        X_scalers_file=os.path.join(phase_model_dir, "X_scalers.npy"),
        Y_scalers_file=os.path.join(phase_model_dir, "Y_scalers.npy"),
    )
    amp_basis = tf.convert_to_tensor(amp_basis, dtype = tf.float32)
    phase_basis = tf.convert_to_tensor(phase_basis, dtype = tf.float32)

    return amp_model, phase_model, amp_basis, phase_basis

def gen_true_waveform(q=2., a1=0.1, a2=0.2, alpha=0.1, delta=0.5, t0=0.2, psi=1.1, phi0=0.):
    params = tf.constant(
        [q, a1, a2, alpha, delta, t0, psi, phi0], dtype=tf.float64, shape=(1, 8)
    )

    params = tf.convert_to_tensor(params)

    y_true = ut.tf_generate_surrogate_at_detector(
        params,
        amp_model=amp_model,
        amp_basis=amp_basis,
        phase_model=phase_model,
        phase_basis=phase_basis,
    )

    return params, y_true


def target_log_prob_fn(x):
    log_like = likelihood.simple_gw_log_like(y_true,
                          x,
                          amp_model= amp_model,
                          amp_basis =amp_basis, 
                          phase_model = phase_model,
                          phase_basis = phase_basis, 
                          dt = dt, 
                          epoch = times[0])
    log_like = tf.cast(log_like, tf.float32)
    log_prior = likelihood.simple_gw_prior(x)
    # tf.print(log_prior)
    prob = log_like + log_prior
    # prob = log_like   
    # tf.print(log_like)
    # tf.print(x)
    # tf.print(min(prob))
    #tf.print(prob[tf.math.argmin(prob)])
    #tf.print(x[tf.math.argmin(prob)])
    # tf.print(tf.multiply(prob, 0.0001))
    return tf.multiply(prob, 0.0001)




# @tf.function(experimental_compile = True, autograph=False)
def do_sampling(
    target_log_prob_fn,
    p0,
    num_results=  5* 10 ** 3,
    num_burnin_steps=2* 10 ** 3,
    ndim=3,
    step_size=0.05,
    sampler = 'HMC',
    bijector = True,
    step_size_adapter = 'simple',
    num_leapfrog_steps = 10,
    max_tree_depth = 8,
    parallel_iterations=20,
    target_accept_prob = 0.7,
    ):
  
    if sampler == 'HMC':
      inner_kernel = tfp.mcmc.SimpleStepSizeAdaptation(
          tfp.mcmc.HamiltonianMonteCarlo(
              target_log_prob_fn = target_log_prob_fn,
              step_size = 0.01,
              num_leapfrog_steps = 10),
          num_adaptation_steps= int(0.75 *num_burnin_steps ))

    elif sampler == 'MCMC':
      inner_kernel = tfp.mcmc.RandomWalkMetropolis(
          target_log_prob_fn = target_log_prob_fn, 
          new_state_fn = tfp.mcmc.random_walk_uniform_fn(scale=0.01,
)
                                                   )
    else: 
      raise ValueError('kernel not implemented')

    if bijector:
      
      kernel=tfp.mcmc.TransformedTransitionKernel(
          inner_kernel= inner_kernel,
          bijector=distribution.bijector)
    
    else : 
       print('not using learnt bijector')
       kernel=tfp.mcmc.TransformedTransitionKernel(
           inner_kernel=inner_kernel,
           bijector=tfb.Identity())
        
    posterior = tfp.mcmc.sample_chain(
        num_results=num_results,
        num_burnin_steps=num_burnin_steps,
        current_state=p0,
        return_final_kernel_results=True,
        kernel=kernel,
    )

    return posterior



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
 

    parser.add_argument("--num-chains", type=int, help="number of parallel chains", default=2)
    parser.add_argument("--annsur-rootdir", type=str, help="path to 3D_NP dir", default="/home/sebastian.khan/data/3D_NP")
    # parser.add_argument("--gpu-devices", type=str,
                        # help="set CUDA_VISIBLE_DEVICES. e.g. '0' or for multiple gpus '0,1,2,3' ")

    args = parser.parse_args()
    args_dict = vars(args)

    logger = init_logger()
    logger.info("running hmc-script")
    # logger.info(f"scrinet version: {scrinet.__version__}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    # logger.info("setting CUDA_VISIBLE_DEVICES")
    # os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_devices

    logger.info(f"printing CUDA_VISIBLE_DEVICES: {os.environ['CUDA_VISIBLE_DEVICES']}")

    check_gpu()

    amp_model, phase_model, amp_basis, phase_basis = load_annsur(args.annsur_rootdir)

    params, y_true = gen_true_waveform(q=2., a1=0.1, a2=0.2, alpha=0.1, delta=0.5, t0=0.2, psi=1.1, phi0=0.)

    psd = tf.ones_like(y_true[0])
    times = np.linspace(-10000, 100, 5000)
    
    dt = times[1] - times[0]
    n = len(times)
    df = 1.0 / (n * dt)
    norm = 4.0 * df
    dt = tf.cast(dt, tf.float32)

    y_true = tf.constant(y_true)
    y_true = y_true + tf.random.normal(mean=0, stddev=0.01, shape=(len(y_true[0]),))

    # plt.figure()
    # plt.plot(y_true.numpy().flatten())
    # plt.savefig('test.png')
    # plt.close()

    # num_chains = 2

    # p0 = [
        # [
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            # tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0]
        # ]
        # for i in range(num_chains)
    # ]

    # p0 = tf.convert_to_tensor(p0)
    # p0 = tf.cast(p0, tf.float32)
    # qs = np.linspace(0.125, 1, 10**2+ 1)

    # probs = []
    # for q in qs:
        # p = params.numpy()
        # p[:,0] = q
        # p = tf.convert_to_tensor(p)
        # p = tf.cast(p, tf.float32)
        # probs.append(target_log_prob_fn(p))


    # plt.figure()
    # plt.plot(qs, probs)
    # plt.savefig('initial-probs.png')
    # plt.close()

    logger.info("setting up initial positions")

    num_chains = args.num_chains

    p0 = [
        [
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0],
            tf.random.uniform(shape=(1,), minval=0.0, maxval=1.0).numpy()[0]
        ]
        for i in range(num_chains)
    ]

    p0 = tf.convert_to_tensor(p0)
    p0 = tf.cast(p0, tf.float32)

    starttime = datetime.datetime.now()
    logger.info('running mcmc')
    mcmc_results = do_sampling(target_log_prob_fn = target_log_prob_fn,
                          p0=p0,
                          num_results=  20 * 10 ** 2,
                          num_burnin_steps= 20* 10 ** 2,
                          sampler = 'MCMC', 
                          bijector = False,
                          )
    endtime = datetime.datetime.now()
    duration = endtime - starttime
    logger.info("mcmc complete")
    logger.info(f"The time cost: {duration}")
    mcmc_trace = mcmc_results.trace
    p_accept = tf.reduce_mean(tf.exp(tf.minimum(mcmc_trace.inner_results.log_accept_ratio, 0.0)))
    # p_accept = tf.reduce_mean(tf.exp(tf.minimum(bij_trace.inner_results.inner_results.log_accept_ratio, 0.0)))
    logger.info(f"Acceptance rate: {p_accept}")

    mcmc_chains = mcmc_results.all_states
    mcmc_data = nn_help.convert_tfp_chains_to_arviz_object(mcmc_chains)

    print(mcmc_chains.shape)

    logger.info("az.summary:")
    print(az.summary(mcmc_data) )


    logger.info("plotting posterior")
    par_names = ["q", "a1", "a2", "alpha", "delta", "t0", "psi", "phi0"]
    for i, val in enumerate(params[0].numpy()):
       plt.figure()
       plt.hist(mcmc_chains.numpy().T[i].flatten(), density=True)
       # sns.distplot(non_bij_chains.numpy().T[i].flatten())
       if i ==0:
           plt.axvline(val / 8, color = 'y', label = 'true')
       else : 
           plt.axvline(val, color = 'y', label = 'true')
       plt.savefig(f"{i}_{par_names[i]}.png")


    logger.info("done!")





