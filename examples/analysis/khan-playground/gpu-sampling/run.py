"""
translating the notebook

https://gitlab.com/SpaceTimeKhantinuum/scrinet/-/blob/making_code_tfable/examples/analysis/khan-playground/observed-waveform-tests.ipynb

into a script to try and run on a gpu
"""

import sys
sys.path.insert(0, '/home/sebastian.khan/git/stk/scrinet/')
import scrinet
print(scrinet.__file__)

import os
from scrinet.workflow.pipe_utils import load_model
from scrinet.sample import sample_helpers
from scrinet.analysis import timeseries_batch as ts
from scrinet.analysis import frequencyseries_batch as fs
from scrinet.sample import likelihood

import matplotlib.pyplot as plt
import numpy as np
import lal

import bilby

import arviz as az


import time

import argparse
import subprocess
import pugna.logger

import tensorflow as tf
import tensorflow_probability as tfp

tfb = tfp.bijectors
tfd = tfp.distributions



def check_gpu():
    logger.info("running 'tf.config.list_physical_devices('GPU')'")
    logger.info(tf.config.list_physical_devices("GPU"))

    try:
        logger.info("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        logger.info("could not run 'nvidia-smi -L'")


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
      try:
        # Currently, memory growth needs to be the same across GPUs
        logger.info("running: tf.config.experimental.set_memory_growth")
        for gpu in gpus:
          tf.config.experimental.set_memory_growth(gpu, True)
      except RuntimeError as e:
          # Memory growth must be set before GPUs have been initialized
          print(e)



def setup_surrogate(user="rg"):
    if user == "sk":
        #         rootdir = "/Users/spx8sk/work/data/scrinet/3D_NP"
        rootdir = "/Users/spx8sk/work/data/scrinet/ann-sur-final-model"
    elif user == "rg":
        rootdir = "/Users/Rhys/Documents/PhD/PE/NN_for_PE/ann-sur-final-model"
    elif user == 'skllodgx1':
        rootdir = "/home/sebastian.khan/data/ann-sur-final-model"

    amp_basis = os.path.join(rootdir, "amp_eim_basis.npy")
    amp_model_dir = os.path.join(rootdir, "results_4_320_relu_Adam")

    phase_basis = os.path.join(rootdir, "phase_eim_basis.npy")
    phase_model_dir = os.path.join(rootdir, "results_4_320_softplus_Adamax")

    amp_model, amp_basis = load_model(
        basis_file=amp_basis,
        nn_weights_file=os.path.join(amp_model_dir, "model.h5"),
        X_scalers_file=os.path.join(amp_model_dir, "X_scalers.npy"),
        Y_scalers_file="",
    )

    phase_model, phase_basis = load_model(
        basis_file=phase_basis,
        nn_weights_file=os.path.join(phase_model_dir, "model.h5"),
        X_scalers_file=os.path.join(phase_model_dir, "X_scalers.npy"),
        Y_scalers_file=os.path.join(phase_model_dir, "y_scalers.npy"),
    )

    amp_basis = tf.convert_to_tensor(amp_basis, dtype=tf.float32)
    phase_basis = tf.convert_to_tensor(phase_basis, dtype=tf.float32)

    return amp_model, amp_basis, phase_model, phase_basis


# @tf.function(experimental_compile=True)
# @tf.function()
def target_log_prob_fn(x):

    x = tf.expand_dims(x, 2)

    wvf_params = sample_helpers.WaveformParams(
        m_tot=x[:, 0],
        mass_ratio=x[:, 1],
        chi_1=x[:, 2],
        chi_2=x[:, 3],
        theta=x[:, 4],
        phi=x[:, 5],
        phase_shift=x[:, 6],
        distance=x[:, 7],
    )

    det_params = sample_helpers.DetectorParams(
        ra=x[:, 8], dec=x[:, 9], psi=x[:, 10], time=x[:, 11]
    )

    log_prior = likelihood.simple_gw_prior(wvf_params, det_params)

    #     tf.print(wvf_params)
    #     tf.print(x)
    #     tf.print(log_prior)

    # remove waveforms that aren't allowed by priors
    cond = tf.math.is_finite(log_prior)

    # to ensure shapes stay the same make a fiducial waveform
    #  this is fine because the prior is still inf
    temp_m_tot = tf.where(tf.reshape(cond, (x.shape[0], 1)), x[:, 0], 60.0)
    temp_q = tf.where(tf.reshape(cond, (x.shape[0], 1)), x[:, 1], 1.0)
    temp_chi_1 = tf.where(tf.reshape(cond, (x.shape[0], 1)), x[:, 2], 0.0)
    temp_chi_2 = tf.where(tf.reshape(cond, (x.shape[0], 1)), x[:, 3], 0.0)

    #     print(new_m_tot)

    adjusted_wvf_params = sample_helpers.WaveformParams(
        m_tot=temp_m_tot,
        mass_ratio=temp_q,
        chi_1=temp_chi_1,
        chi_2=temp_chi_2,
        theta=x[:, 4],
        phi=x[:, 5],
        phase_shift=x[:, 6],
        distance=x[:, 7],
    )

    log_like = likelihood.log_likelihood(
        signal,
        adjusted_wvf_params,
        det_params,
        f_lower=f_lower,
        delta_f=delta_f,
        seglen=seglen,
        sample_rate=sample_rate,
        delta_t=delta_t,
        amp_model=amp_model,
        amp_basis=amp_basis,
        phase_model=phase_model,
        phase_basis=phase_basis,
        psd=None,
        detector="H1",
    )

    #     log_prior = likelihood.simple_gw_prior(wvf_params, det_params)
    prob = log_like + tf.cast(log_prior, tf.float32)
    #     prob = log_like + log_prior
    prob = tf.cast(prob, tf.float64)
    return prob * 10 ** -5



if __name__ == "__main__":



    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # parser.add_argument("--output-dir", type=str,
                        # help="output directory", required=True)
    parser.add_argument("-v", "--verbose",
                        help="""
                        increase output verbosity
                        no -v: WARNING
                        -v: INFO
                        -vv: DEBUG
                        """,
                        action='count', default=0)


    parser.add_argument("--gpu-devices", type=str,
                        help="set CUDA_VISIBLE_DEVICES. e.g. '0'")

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = pugna.logger.init_logger(level=level)
    logger.info("running pugna_fit")
    logger.info(f"pugna version: {pugna.__version__}")
    logger.info(f"verbosity turned on at level: {level}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    if args.gpu_devices:
        logger.info("setting CUDA_VISIBLE_DEVICES")
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_devices
        check_gpu()
        set_gpu_memory_growth()


    logger.info("loading ann-sur model")
    amp_model, amp_basis, phase_model, phase_basis = setup_surrogate(user="skllodgx1") 

    x = tf.constant([
        [4,0,0],
        [6,0,0],
        [6,1,1]
    ], dtype=np.float32)
    theta = tf.constant([[np.pi/3], [np.pi/2], [np.pi/3]])
    phi = tf.constant([[np.pi], [np.pi/2], [np.pi/8]])
    mtot = tf.constant([[70.], [60.], [120.]])
    dist = tf.constant([[2.3],[2.3],[2.3]])
    phase_shift = tf.constant([[1.],[2.],[-1.23]])
    
    
    f_lower = 20.
    
    
    seglen = 4.
    # seglen = 8.
    
    # this is actually dt...
    sample_rate = 2048.*2
    
    wvf_params = sample_helpers.WaveformParams(
        distance=dist,
        m_tot=mtot,
        mass_ratio=tf.reshape(x[:, 0], (-1, 1)),
        chi_1=tf.reshape(x[:, 1], (-1, 1)),
        chi_2=tf.reshape(x[:, 2], (-1, 1)),
        theta=theta,
        phi=phi,
        phase_shift=phase_shift,
    )
    
    # # f_lower and sample-rate should always be constant and so can just be scalars (0-D Tensor)
    (
        surr_times_sec,
        hplus,
        hcross,
        unrolled_peak_time,
    ) = sample_helpers.generate_surrogate_hp_hc(
        waveform_params=wvf_params,
        f_lower=f_lower,
        seglen=seglen,
        sample_rate=sample_rate,
        amp_model=amp_model,
        amp_basis=amp_basis,
        phase_model=phase_model,
        phase_basis=phase_basis,
    )
    
    ra = tf.cast(tf.ones_like(dist), tf.float64)
    dec = tf.cast(tf.ones_like(dist), tf.float64)
    psi = tf.cast(tf.ones_like(dist), tf.float64)
    time = tf.cast(
        tf.constant(
            [
                [
                    3.0,
                ],
                [2.0],
                [3.0],
            ]
        ),
        tf.float64,
    )
    
    det_params = sample_helpers.DetectorParams(ra=ra, dec=dec, psi=psi, time=time)
    
    t = 2
    f_lower = 20.0
    
    H1_det_wf = sample_helpers.tf_generate_surrogate_at_detector(
        waveform_params=wvf_params,
        detector_params=det_params,
        f_lower=f_lower,
        seglen=seglen,
        sample_rate=sample_rate,
        amp_model=amp_model,
        amp_basis=amp_basis,
        phase_model=phase_model,
        phase_basis=phase_basis,
        detector="H1",
    )
    
    L1_det_wf = sample_helpers.tf_generate_surrogate_at_detector(
        waveform_params=wvf_params,
        detector_params=det_params,
        f_lower=f_lower,
        seglen=seglen,
        sample_rate=sample_rate,
        amp_model=amp_model,
        amp_basis=amp_basis,
        phase_model=phase_model,
        phase_basis=phase_basis,
        detector="L1",
    )
    
    delta_t = H1_det_wf.delta_t
    #delta_t = 1./sample_rate
    
    # convert to float64
    h1 = []
    l1 = []
    for i in range(H1_det_wf.data.shape[0]):
        h1.append(tf.cast(H1_det_wf.data[i], tf.float64))
        l1.append(tf.cast(L1_det_wf.data[i], tf.float64))
    h1 = tf.convert_to_tensor(h1)
    l1 = tf.convert_to_tensor(l1)
    
    H1_det_wf = ts.TimeSeries(h1, delta_t=delta_t)
    L1_det_wf = ts.TimeSeries(l1, delta_t=delta_t)
    
    # initialise a single interferometer representing LIGO Hanford
    H1 = bilby.gw.detector.get_empty_interferometer("H1")
    # set the strain data at the interferometer
    H1.set_strain_data_from_power_spectral_density(
        sampling_frequency=sample_rate, duration=seglen
    )
    
    delta_f = 1.0 / seglen
    
    # from pycbc
    # Dynamic range factor: a large constant for rescaling
    # GW strains.  This is 2**69 rounded to 17 sig.fig.
    
    DYN_RANGE_FAC = 5.9029581035870565e20
    
    psd = fs.FrequencySeries(
        DYN_RANGE_FAC * DYN_RANGE_FAC * H1.power_spectral_density_array.reshape(1, -1),
        delta_f=delta_f,
        dtype=tf.float64,
    )
    
    signal_plus_noise = H1_det_wf.data[0] + np.random.normal(
        scale=0.000001, size=H1_det_wf.data.shape[1]
    )
    signal_plus_noise = ts.TimeSeries(
        tf.reshape(signal_plus_noise, (1, -1)), delta_t=delta_t
    )
    
    template = fs.FrequencySeries(H1_det_wf.to_frequencyseries().data, delta_f=delta_f)
    # signal = fs.FrequencySeries(tf.expand_dims(H1_det_wf.to_frequencyseries().data[0],0), delta_f=delta_f)
    signal = fs.FrequencySeries(
        tf.expand_dims(signal_plus_noise.to_frequencyseries().data[0], 0), delta_f=delta_f
    )
    
    
    
    
    params = [
        wvf_params.m_tot,
        #     tf.constant([[70], [60], [60]]),
        wvf_params.mass_ratio,
        wvf_params.chi_1,
        wvf_params.chi_2,
        wvf_params.theta,
        wvf_params.phi,
        wvf_params.phase_shift,
        wvf_params.distance,
        det_params.ra,
        det_params.dec,
        det_params.psi,
        det_params.time,
    ]
    
    
    params = [tf.cast(param, tf.float64) for param in params]
    params = tf.transpose(
        tf.reshape(
            params,
            (12, 3),
        )
    )


    # qs = np.linspace(1, 8, 151)
    # Ms = np.linspace(40, 120, 21)

    # probs = []
    # for m in Ms:
        # p = params.numpy()
        # p[0, 0] = m
        # p = tf.convert_to_tensor(p)
        # p = tf.cast(p, tf.float64)
        # #     print(p)
        # #     print(m)
        # # t_start = time.time()
        # print(target_log_prob_fn(p))
        # probs.append(target_log_prob_fn(p)[0])
        # # print(f" q = {m}: evaluation took {time.time() - t_start}")

    # plt.plot(Ms, probs)
    # plt.xlabel("M", fontsize=(20))
    # plt.ylabel("log L(M)", fontsize=(20))
    # # plt.xlim(3.8, 4.2)
    # plt.savefig('example-likelihood.png')
    # plt.close()





    # sampling
    def make_mcmc_kernel_fn(target_log_prob_fn):
        return tfp.mcmc.RandomWalkMetropolis(
            target_log_prob_fn=target_log_prob_fn,
            new_state_fn=tfp.mcmc.random_walk_normal_fn(
                scale=step_size,
            ),
        )
    
    
    def make_hmc_kernel_fn(target_log_prob_fn):
        return tfp.mcmc.HamiltonianMonteCarlo(
            target_log_prob_fn=target_log_prob_fn, step_size=step_size, num_leapfrog_steps=3
        )


    # @tf.function(experimental_compile=True)
    @tf.function()
    def do_sampling(
        target_log_prob_fn,
        p0,
        num_results=8 * 10 ** 2,
        num_burnin_steps=8 * 10 ** 2,
        ndim=3,
        step_size=0.01,
        sampler="MCMC",
        bijector=True,
        step_size_adapter="simple",
        num_leapfrog_steps=10,
        max_tree_depth=4,
        parallel_iterations=20,
        target_accept_prob=0.7,
    ):
    
        if sampler == "HMC":
            inner_kernel = tfp.mcmc.DualAveragingStepSizeAdaptation(
                tfp.mcmc.HamiltonianMonteCarlo(
                    target_log_prob_fn=target_log_prob_fn,
                    step_size=step_size,
                    num_leapfrog_steps=5,
                ),
                num_adaptation_steps=int(0.75 * num_burnin_steps),
            )
    
        elif sampler == "NUTS":
            inner_kernel = tfp.mcmc.DualAveragingStepSizeAdaptation(
                tfp.mcmc.NoUTurnSampler(
                    target_log_prob_fn=target_log_prob_fn,
                    step_size=step_size,
                    max_tree_depth=max_tree_depth,
                ),
                num_adaptation_steps=int(0.75 * num_burnin_steps),
            )
    
        elif sampler == "MCMC":
            inner_kernel = tfp.mcmc.RandomWalkMetropolis(
                target_log_prob_fn=target_log_prob_fn,
                new_state_fn=tfp.mcmc.random_walk_normal_fn(
                    scale=step_size,
                ),
            )
    
        elif sampler == "PTMCMC":
            inner_kernel = tfp.mcmc.RandomWalkMetropolis(
                target_log_prob_fn=target_log_prob_fn,
                new_state_fn=tfp.mcmc.random_walk_normal_fn(
                    scale=step_size,
                ),
            )
            dtype = tf.float64
            inverse_temperatures = 0.5 ** tf.range(4, dtype=dtype)
    
            inner_kernel = tfp.mcmc.ReplicaExchangeMC(
                target_log_prob_fn=target_log_prob_fn,
                inverse_temperatures=inverse_temperatures,
                make_kernel_fn=make_mcmc_kernel_fn,
            )
        
        elif sampler == "PTHMC":
            inner_kernel = tfp.mcmc.RandomWalkMetropolis(
                target_log_prob_fn=target_log_prob_fn,
                new_state_fn=tfp.mcmc.random_walk_normal_fn(
                    scale=step_size,
                ),
            )
            dtype = tf.float64
            inverse_temperatures = 0.5 ** tf.range(4, dtype=dtype)
    
            inner_kernel = tfp.mcmc.ReplicaExchangeMC(
                target_log_prob_fn=target_log_prob_fn,
                inverse_temperatures=inverse_temperatures,
                make_kernel_fn=make_hmc_kernel_fn,
            )
    
        else:
            raise ValueError("kernel not implemented")
    
        if bijector:
    
            kernel = tfp.mcmc.TransformedTransitionKernel(
                inner_kernel=inner_kernel, bijector=distribution.bijector
            )
    
        else:
            print("not using learnt bijector")
            kernel = tfp.mcmc.TransformedTransitionKernel(
                inner_kernel=inner_kernel, bijector=tfb.Identity()
            )
    
        posterior = tfp.mcmc.sample_chain(
            num_results=num_results,
            num_burnin_steps=num_burnin_steps,
            current_state=p0,
            return_final_kernel_results=True,
            kernel=kernel,
        )
    
        return posterior


    num_chains = 3
    num_results = int(3 * 10 ** 2)
    num_burnin_steps = int(2 * 10 ** 2)
    p0 = params[0]
    p0 = p0 + abs(np.random.normal(0, 0.0000001, size=(num_chains, len(params[0]))))

    p0 = tf.convert_to_tensor(p0)
    p0 = tf.cast(p0, tf.float64)

    step_size = 1 * 10 ** -3 * abs(p0[0]) + 0.0005

    # step_size = tf.constant(
        # [0.01, 0.05, 0.0, 0.0, 0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0001],
        # dtype=step_size.dtype,
    # )

    step_size = tf.constant(
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0001],
        dtype=step_size.dtype,
    )



    param_names = [
        "m_tot",
        "mass_ratio",
        "chi_1",
        "chi_2",
        "theta",
        "phi",
        "phase_shift",
        "distance",
        "ra",
        "dec",
        "psi",
        "time",
    ]
    
    logger.info('do sampling')
    posterior = do_sampling(target_log_prob_fn,
                            sampler = 'HMC',
                            num_results =num_results,
                            num_burnin_steps = num_burnin_steps,
                            p0=p0,
                            step_size = step_size,
                            bijector=False)
    logger.info('do sampling done')
    
    
    mcmc_trace = posterior.trace
    
    try : 
        p_accept = tf.reduce_mean(
        tf.exp(tf.minimum(mcmc_trace.inner_results.inner_results.log_accept_ratio, 0.0))
        )
    
    except AttributeError: 
        p_accept = tf.reduce_mean(
            tf.exp(tf.minimum(mcmc_trace.inner_results.log_accept_ratio, 0.0))
        )
    
    print(f"Acceptance rate: {p_accept}")


    
    np.save('pos.npy', posterior.all_states.numpy())


    mcmc_chains = posterior.all_states[:, :, -1:]
    # mcmc_chains = posterior.all_states[:, 0:2]
    mcmc_data = sample_helpers.convert_tfp_chains_to_arviz_object(mcmc_chains, param_names[-1:])
    
    print(az.summary(mcmc_data))
    
    axes = az.plot_posterior(mcmc_data) 
    fig = axes.ravel()[0].figure
    fig.savefig('posplot.png')

    axes = az.plot_trace(mcmc_data) 
    fig = axes.ravel()[0].figure
    fig.savefig('postraceplot.png')

