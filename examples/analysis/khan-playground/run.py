#!/usr/bin/env python
# coding: utf-8

import datetime
import arviz as az
import tensorflow_probability as tfp
import tensorflow as tf


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            print("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)


set_gpu_memory_growth()

import numpy as np
import bilby
from scrinet.sample import likelihood
from scrinet.analysis import frequencyseries_batch as fs
import os
from scrinet.workflow.pipe_utils import load_model
from scrinet.sample import sample_helpers
from scrinet.analysis import timeseries_batch as ts
from scrinet.sample.tmp_SoftClip import SoftClip
from scrinet.analysis import matchedfilter_batch as mf
import matplotlib
import matplotlib.pyplot as plt

# tf.config.experimental.enable_tensor_float_32_execution(False)
matplotlib.use("agg")
az.rcParams["plot.max_subplots"] = 200

tfb = tfp.bijectors
tfd = tfp.distributions

image_dir = '/home/rhys.green/run/mcmc_plots/'
data_dir = '/home/rhys.green/run/data/'


def setup_surrogate(user="lhodgx1"):
    if user == "sk":
        #         rootdir = "/Users/spx8sk/work/data/scrinet/3D_NP"
        rootdir = "/Users/spx8sk/work/data/scrinet/ann-sur-final-model"
    elif user == "rg":
        rootdir = "/Users/Rhys/Documents/PhD/PE/NN_for_PE/ann-sur-final-model"
    elif user == "lhodgx1":
        rootdir = "/home/sebastian.khan/projects/scrinet/gpu/run/ann-sur-final-model"

    amp_basis = os.path.join(rootdir, "amp_eim_basis.npy")
    amp_model_dir = os.path.join(rootdir, "results_4_320_relu_Adam")

    phase_basis = os.path.join(rootdir, "phase_eim_basis.npy")
    phase_model_dir = os.path.join(rootdir, "results_4_320_softplus_Adamax")

    amp_model, amp_basis = load_model(
        basis_file=amp_basis,
        nn_weights_file=os.path.join(amp_model_dir, "model.h5"),
        X_scalers_file=os.path.join(amp_model_dir, "X_scalers.npy"),
        Y_scalers_file="",
    )

    phase_model, phase_basis = load_model(
        basis_file=phase_basis,
        nn_weights_file=os.path.join(phase_model_dir, "model.h5"),
        X_scalers_file=os.path.join(phase_model_dir, "X_scalers.npy"),
        Y_scalers_file=os.path.join(phase_model_dir, "y_scalers.npy"),
    )

    amp_basis = tf.convert_to_tensor(amp_basis, dtype=tf.float32)
    phase_basis = tf.convert_to_tensor(phase_basis, dtype=tf.float32)

    return amp_model, amp_basis, phase_model, phase_basis


# @tf.function(experimental_compile=True)
def target_log_prob_fn(x_original):
    num_chains = x_original.shape[0]
    x = tf.expand_dims(x_original, 2)
    scaled_wvf_params = sample_helpers.WaveformParams(
        m_tot=x[:, 0],
        mass_ratio=x[:, 1],
        chi_1=x[:, 2],
        chi_2=x[:, 3],
        theta=x[:, 4],
        phi=x[:, 5],
        phase_shift=x[:, 6],
        distance=x[:, 7],
    )

    det_params = sample_helpers.DetectorParams(
        ra=x[:, 8], dec=x[:, 9], psi=x[:, 10], time=x[:, 11]
    )

    log_prior = likelihood.simple_gw_prior(scaled_wvf_params, det_params)
    cond = tf.reshape(tf.math.is_finite(log_prior), (num_chains, 1))

    # to ensure shapes stay the same make a fiducial waveform
    #  this is fine because the prior is still inf
    temp_m_tot = tf.where(cond, x[:, 0], 0.6)
    temp_q = tf.where(cond, x[:, 1], 0.1)
    temp_chi_1 = tf.where(cond, x[:, 2], 0.0)
    temp_chi_2 = tf.where(cond, x[:, 3], 0.0)

    adjusted_wvf_params = sample_helpers.WaveformParams(
        m_tot=100. * temp_m_tot,
        mass_ratio=8 * temp_q,
        chi_1=temp_chi_1,
        chi_2=temp_chi_2,
        theta=2 * np.pi * x[:, 4],
        phi=2 * np.pi * x[:, 5],
        phase_shift=0. * x[:, 6],
        distance=8. * 10 ** 3 * x[:, 7],
    )

    adjusted_det_params = sample_helpers.DetectorParams(
        ra=2 * np.pi * x[:, 8],
        dec=np.pi / 2 * x[:, 9],
        psi=2 * np.pi * x[:, 10],
        time=3. * x[:, 11]
    )

    log_like = likelihood.log_likelihood(
        signal,
        adjusted_wvf_params,
        adjusted_det_params,
        n_batch=num_chains,
        f_lower=f_lower,
        delta_f=delta_f,
        seglen=seglen,
        sample_rate=sample_rate,
        delta_t=delta_t,
        amp_model=amp_model,
        amp_basis=amp_basis,
        phase_model=phase_model,
        phase_basis=phase_basis,
        psd=psd,
        detector="H1",
    )

    prob = log_like + tf.cast(log_prior, tf.float32)
    prob = tf.cast(prob, tf.float64)
    return prob


@tf.function()
def elbo_loss():
    q_samples = distribution.sample((256,))
    return -tf.reduce_mean(
        target_log_prob_fn(q_samples) - distribution.log_prob(q_samples)
    )


def make_mcmc_kernel_fn(target_log_prob_fn):
    return tfp.mcmc.RandomWalkMetropolis(
        target_log_prob_fn=target_log_prob_fn,
        new_state_fn=tfp.mcmc.random_walk_normal_fn(
            scale=step_size,
        ),
    )


def make_hmc_kernel_fn(target_log_prob_fn):
    return tfp.mcmc.HamiltonianMonteCarlo(
        target_log_prob_fn=target_log_prob_fn, step_size=step_size, num_leapfrog_steps=3
    )


@tf.function(experimental_compile=True)
def do_sampling(
        target_log_prob_fn,
        p0,
        num_results=8 * 10 ** 2,
        num_burnin_steps=8 * 10 ** 2,
        step_size=0.01,
        sampler="MCMC",
        bijector='Constraint',
        num_leapfrog_steps=8,
        max_tree_depth=7,
        target_accept_prob=0.65,
):
    if sampler == "HMC":
        print('Sampling using HMC')
        inner_kernel = tfp.mcmc.DualAveragingStepSizeAdaptation(
            inner_kernel=tfp.mcmc.HamiltonianMonteCarlo(
                target_log_prob_fn=target_log_prob_fn,
                step_size=step_size,
                num_leapfrog_steps=num_leapfrog_steps,
            ),
            target_accept_prob=target_accept_prob,
            num_adaptation_steps=int(0.75 * num_burnin_steps),
            reduce_fn=tfp.math.reduce_log_harmonic_mean_exp,
        )

    elif sampler == "NUTS":
        print('Sampling using NUTS')
        inner_kernel = tfp.mcmc.DualAveragingStepSizeAdaptation(
            tfp.mcmc.NoUTurnSampler(
                target_log_prob_fn=target_log_prob_fn,
                step_size=step_size,
                max_tree_depth=max_tree_depth,
            ),
            target_accept_prob=target_accept_prob,
            num_adaptation_steps=int(0.75 * num_burnin_steps),
            # reduce_fn=tfp.math.reduce_log_harmonic_mean_exp,
        )

    elif sampler == "MCMC":
        print('Sampling using Random walk')
        inner_kernel = tfp.mcmc.RandomWalkMetropolis(
            target_log_prob_fn=target_log_prob_fn,
            new_state_fn=tfp.mcmc.random_walk_normal_fn(
                scale=step_size,
            ),
        )

    elif sampler == "PTMCMC":
        dtype = tf.float64
        inverse_temperatures = 0.5 ** tf.range(4, dtype=dtype)

        inner_kernel = tfp.mcmc.ReplicaExchangeMC(
            target_log_prob_fn=target_log_prob_fn,
            inverse_temperatures=inverse_temperatures,
            make_kernel_fn=make_mcmc_kernel_fn,
        )

    elif sampler == "PTHMC":
        dtype = tf.float64
        inverse_temperatures = 0.5 ** tf.range(4, dtype=dtype)

        inner_kernel = tfp.mcmc.ReplicaExchangeMC(
            target_log_prob_fn=target_log_prob_fn,
            inverse_temperatures=inverse_temperatures,
            make_kernel_fn=make_hmc_kernel_fn,
        )

    else:
        raise ValueError("kernel not implemented")

    if bijector == 'NN':
        kernel = tfp.mcmc.TransformedTransitionKernel(
            inner_kernel=inner_kernel, bijector=distribution.bijector
        )

    elif bijector == 'Constraint':
        kernel = tfp.mcmc.TransformedTransitionKernel(
            inner_kernel=inner_kernel,
            bijector=constraints,
        )
    else:
        print("not using bijector")
        kernel = tfp.mcmc.TransformedTransitionKernel(
            inner_kernel=inner_kernel,
            bijector=tfb.Identity(),
        )

    posterior = tfp.mcmc.sample_chain(
        num_results=num_results,
        num_burnin_steps=num_burnin_steps,
        current_state=p0,
        return_final_kernel_results=True,
        kernel=kernel,
        num_steps_between_results=10
    )

    return posterior


amp_model, amp_basis, phase_model, phase_basis = setup_surrogate(user="lhodgx1")

x = tf.constant([[2., 0, 0], [4., 0, 0], [6, 1, 1]], dtype=np.float32)
theta = tf.constant([[np.pi / 3], [np.pi / 2], [np.pi / 3]])
phi = tf.constant([[np.pi], [np.pi / 2], [np.pi / 8]])
mtot = tf.constant([[70.0], [70.0], [80.0]])
dist = tf.constant([[5.0 * (10 ** 3)], [5.0 * 10 ** 3], [5.0 * 10 ** 3]])
phase_shift = tf.constant([[0.], [0.], [0.]])

f_lower = 30.0

seglen = 4.0

sample_rate = 2048.0 * 2

ra = tf.cast(tf.ones_like(dist), tf.float64)
dec = tf.cast(tf.ones_like(dist), tf.float64)
psi = tf.cast(tf.ones_like(dist), tf.float64)
time = tf.cast(
    tf.constant(
        [
            [
                3.0,
            ],
            [2.0],
            [2.5],
        ]
    ),
    tf.float64,
)

wvf_params = sample_helpers.WaveformParams(
    distance=dist,
    m_tot=mtot,
    mass_ratio=tf.reshape(x[:, 0], (-1, 1)),
    chi_1=tf.reshape(x[:, 1], (-1, 1)),
    chi_2=tf.reshape(x[:, 2], (-1, 1)),
    theta=theta,
    phi=phi,
    phase_shift=phase_shift,
)

det_params = sample_helpers.DetectorParams(ra=ra, dec=dec, psi=psi, time=time)

# # f_lower and sample-rate should always be constant and so can just be scalars (0-D Tensor)
(
    surr_times_sec,
    hplus,
    hcross,
    unrolled_peak_time,
) = sample_helpers.generate_surrogate_hp_hc(
    waveform_params=wvf_params,
    n_batch=x.shape[0],
    f_lower=f_lower,
    seglen=seglen,
    sample_rate=sample_rate,
    amp_model=amp_model,
    amp_basis=amp_basis,
    phase_model=phase_model,
    phase_basis=phase_basis,
)

t = 2

H1_det_wf = sample_helpers.tf_generate_surrogate_at_detector(
    waveform_params=wvf_params,
    detector_params=det_params,
    n_batch=x.shape[0],
    f_lower=f_lower,
    seglen=seglen,
    sample_rate=sample_rate,
    amp_model=amp_model,
    amp_basis=amp_basis,
    phase_model=phase_model,
    phase_basis=phase_basis,
    detector="H1",
)

# L1_det_wf = sample_helpers.tf_generate_surrogate_at_detector(
#     waveform_params=wvf_params,
#     detector_params=det_params,
#     n_batch=x.shape[0],
#     f_lower=f_lower,
#     seglen=seglen,
#     sample_rate=sample_rate,
#     amp_model=amp_model,
#     amp_basis=amp_basis,
#     phase_model=phase_model,
#     phase_basis=phase_basis,
#     detector="L1",
# )

delta_t = surr_times_sec[1] - surr_times_sec[0]

H1_det_wf = ts.TimeSeries(H1_det_wf, delta_t=delta_t)
# L1_det_wf = ts.TimeSeries(L1_det_wf, delta_t=delta_t)

# # trying likelihood
# initialise a single interferometer representing LIGO Hanford
H1 = bilby.gw.detector.get_empty_interferometer("H1")
# set the strain data at the interferometer
H1.set_strain_data_from_power_spectral_density(
    sampling_frequency=sample_rate, duration=seglen
)

delta_f = 1.0 / seglen

# Dynamic range factor: a large constant for rescaling
# GW strains.  This is 2**69 rounded to 17 sig.fig.

DYN_RANGE_FAC = 5.9029581035870565e20
# DYN_RANGE_FAC = 1.0

psd = fs.FrequencySeries(
    tf.cast(
        1 * 10 ** 2 * DYN_RANGE_FAC * DYN_RANGE_FAC * H1.power_spectral_density_array.reshape(1, -1),
        tf.float32,
    ),
    delta_f=delta_f,
    dtype=tf.float32,
)

tf.cast(H1.power_spectral_density_array.reshape(1, -1), tf.float32)

signal_plus_noise = H1_det_wf.data[0] + np.random.normal(
    scale=0.1, size=H1_det_wf.data.shape[1]
)
signal_plus_noise = ts.TimeSeries(
    tf.reshape(signal_plus_noise, (1, -1)), delta_t=delta_t
)

template = fs.FrequencySeries(H1_det_wf.to_frequencyseries().data, delta_f=delta_f)
signal = fs.FrequencySeries(
    tf.expand_dims(signal_plus_noise.to_frequencyseries().data[0], 0), delta_f=delta_f
)

snr = mf.matched_filter(signal,
                        signal,
                        psd,
                        low_frequency_cutoff=f_lower).data[0].numpy().max()

print(f'SNR = {snr}')

num_chains = 2 ** 5
#
raise ValueError
param_names = [
    "m_tot",
    "mass_ratio",
    "chi_1",
    "chi_2",
    "theta",
    "phi",
    "phase_shift",
    "distance",
    "ra",
    "dec",
    "psi",
    "time",
]

fixed_params = ["phase_shift", "time"]

wvf_param_dict = wvf_params._asdict()
det_param_dict = det_params._asdict()

inj_params = [
    wvf_params.m_tot,
    wvf_params.mass_ratio,
    wvf_params.chi_1,
    wvf_params.chi_2,
    wvf_params.theta,
    wvf_params.phi,
    wvf_params.phase_shift,
    wvf_params.distance,
    det_params.ra,
    det_params.dec,
    det_params.psi,
    det_params.time,
]

inj_params = [tf.cast(param, tf.float64) for param in inj_params]
inj_params = tf.transpose(
    tf.reshape(
        inj_params,
        (12, 3),
    )
)

inj = inj_params[0]
inj = inj + abs(
    np.random.normal(
        0, 0.0, size=(num_chains, len(inj_params[0]))
    )
)

priors = likelihood.GwPriors()
prior_dict = priors._asdict()

p0_dict = {}
for i, prior in enumerate(priors):
    p0_dict[priors._fields[i]] = prior.sample(num_chains, )

param_scalers = sample_helpers.GwParamScalers()._asdict()

for param in fixed_params:
    p0_dict[param] = 1. / param_scalers[param] * inj[:, param_names.index(param)]

p0_params = [
    p0_dict['m_tot'],
    p0_dict['mass_ratio'],
    p0_dict['chi_1'],
    p0_dict['chi_2'],
    p0_dict['theta'],
    p0_dict['phi'],
    p0_dict['phase_shift'],
    p0_dict['distance'],
    p0_dict['ra'],
    p0_dict['dec'],
    p0_dict['psi'],
    p0_dict['time'],
]

p0_params = [tf.cast(param, tf.float64) for param in p0_params]

p0 = tf.transpose(
    tf.reshape(
        p0_params,
        (12, num_chains),
    )
)

step_size = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.01]
step_size = np.array(step_size) * 0.5

for param in fixed_params:
    step_size[param_names.index(param)] = 0.

step_size = tf.constant(step_size)

step_size = tf.repeat(tf.expand_dims(step_size, 0), num_chains, axis=0)

# lower_bounds = [0.6, 0.125, -1.0, -1.0, 0.0, 0.0, 0.0, 0.125, 0.0, -1, 0.0, 0.95]
# upper_bounds = [1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.05, ]
#
# constraints = SoftClip(
#     low=tf.cast(lower_bounds, tf.float64, ),
#     high=tf.cast(upper_bounds, tf.float64, ),
#     hinge_softness=0.01
# )

param_scalers = sample_helpers.GwParamScalers()._asdict()

num_results = int(1 * 10 ** 4)
num_burnin_steps = int(5 * 10 ** 3)
sampler = "HMC"
print("starting inference")
print(target_log_prob_fn(p0))
starttime = datetime.datetime.now()
posterior = do_sampling(
    target_log_prob_fn,
    sampler=sampler,
    num_results=num_results,
    num_burnin_steps=num_burnin_steps,
    p0=p0,
    step_size=step_size,
    bijector=None
)

endtime = datetime.datetime.now()
duration = endtime - starttime
print("inference complete")
print(f"inference duration: {duration}")

# raise ValueError

mcmc_trace = posterior.trace

try:
    p_accept = tf.reduce_mean(
        tf.exp(tf.minimum(mcmc_trace.inner_results.inner_results.log_accept_ratio, 0.0))
    )

except AttributeError:
    p_accept = tf.reduce_mean(
        tf.exp(tf.minimum(mcmc_trace.inner_results.log_accept_ratio, 0.0))
    )

print(f"Acceptance rate: {p_accept}")

mcmc_chains = posterior.all_states
mcmc_data = sample_helpers.convert_tfp_chains_to_arviz_object(
    posterior.all_states, param_names, ["m_tot",
                                        "mass_ratio",
                                        "chi_1",
                                        "chi_2",
                                        "theta",
                                        "phi",
                                        "distance",
                                        "ra",
                                        "dec",
                                        "psi",
                                        # "time",
                                        ]
)
# print(mcmc_trace.inner_results. new_step_size[-1])
mcmc_data.to_netcdf(f"{data_dir}{sampler}_samples.nc")
print('MCMC Sunmmary')
print(az.summary(mcmc_data))
