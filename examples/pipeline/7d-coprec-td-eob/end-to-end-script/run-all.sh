# PARAMS

# DATA_TO_SAVE="time amp phase freq real imag"
# DATA_TO_SAVE="time amp phase real imag"
# DATA_TO_SAVE="time amp phase"
DATA_TO_SAVE="time amp phase alpha beta gamma"

DATA1=amp
DATA2=phase
DATA3=alpha
DATA4=beta
DATA5=gamma

QMIN=1
QMAX=2

CHI1MIN=0.1
CHI1MAX=0.4

THETA1MIN=0.01745
THETA1MAX=1.0472

PHI1MIN=0.
PHI1MAX=6.28318531

CHI2MIN=0.1
CHI2MAX=0.4

THETA2MIN=0.01745
THETA2MAX=1.0472

PHI2MIN=0.
PHI2MAX=6.28318531

OUTPUT_DIR=ouput

BATCH_SIZE=10

INPUT_UNITS="128"
HIDDEN_UNITS="128 128 128 128 128"

# INPUT_UNITS="64"
# HIDDEN_UNITS="64 64 64 64 64 64 64 64 64 64 64 64 64"

# INPUT_UNITS="64"
# HIDDEN_UNITS="64 128 256 512 256 128 64"
LEARN_RATE=0.001
# LEARN_RATE=0.0001
#LEARN_RATE=0.000001
# LEARN_RATE=0.0005

# EPOCHS=2000
#EPOCHS=1000
EPOCHS=10
# EPOCHS=500
# EPOCHS=6000

LR_SCH=""
# LR_SCH="--lr-schedule"

USE_ALR="--use-alr"
ALR_VERBOSE=1
ALR_MIN_LR="1e-5"
ALR_PATIENCE=100
ALR_FACTOR=0.2

USE_ES="--use-es"
ES_VERBOSE=1
ES_PATIENCE=1000

KERNEL_INITIALIZER="he_uniform"
# KERNEL_INITIALIZER="he_normal"
# KERNEL_INITIALIZER="glorot_uniform"

ACTIVATION='relu'
# ACTIVATION='elu'
# ACTIVATION='tanh'

COMMON_ARGS="--q-min ${QMIN} --q-max ${QMAX} --chi1-min ${CHI1MIN} --chi1-max ${CHI1MAX} --theta1-min ${THETA1MIN} --theta1-max ${THETA1MAX} --phi1-min ${PHI1MIN} --phi1-max ${PHI1MAX} --chi2-min ${CHI2MIN} --chi2-max ${CHI2MAX} --theta2-min ${THETA2MIN} --theta2-max ${THETA2MAX} --phi2-min ${PHI2MIN} --phi2-max ${PHI2MAX}"

echo ${COMMON_ARGS}

# gen wf data
scrinet_gen_wf_data_7d_prec_single_spin_coprec --grid random --npts 4 -v --n-cores 4 --output-dir ${OUTPUT_DIR}/seed_wf_data --q-min ${QMIN} --q-max ${QMAX} --chi1-min ${CHI1MIN} --chi1-max ${CHI1MAX} --theta1-min ${THETA1MIN} --theta1-max ${THETA1MAX} --phi1-min ${PHI1MIN} --phi1-max ${PHI1MAX} --chi2-min ${CHI2MIN} --chi2-max ${CHI2MAX} --theta2-min ${THETA2MIN} --theta2-max ${THETA2MAX} --phi2-min ${PHI2MIN} --phi2-max ${PHI2MAX} --data-to-save ${DATA_TO_SAVE}
scrinet_gen_wf_data_7d_prec_single_spin_coprec --grid random --npts 10 -v --n-cores 4 --output-dir ${OUTPUT_DIR}/train_wf_data --q-min ${QMIN} --q-max ${QMAX} --chi1-min ${CHI1MIN} --chi1-max ${CHI1MAX} --theta1-min ${THETA1MIN} --theta1-max ${THETA1MAX} --phi1-min ${PHI1MIN} --phi1-max ${PHI1MAX} --chi2-min ${CHI2MIN} --chi2-max ${CHI2MAX} --theta2-min ${THETA2MIN} --theta2-max ${THETA2MAX} --phi2-min ${PHI2MIN} --phi2-max ${PHI2MAX} --data-to-save ${DATA_TO_SAVE}
scrinet_gen_wf_data_7d_prec_single_spin_coprec --grid random --npts 10 -v --n-cores 4 --output-dir ${OUTPUT_DIR}/validation_wf_data --q-min ${QMIN} --q-max ${QMAX} --chi1-min ${CHI1MIN} --chi1-max ${CHI1MAX} --theta1-min ${THETA1MIN} --theta1-max ${THETA1MAX} --phi1-min ${PHI1MIN} --phi1-max ${PHI1MAX} --chi2-min ${CHI2MIN} --chi2-max ${CHI2MAX} --theta2-min ${THETA2MIN} --theta2-max ${THETA2MAX} --phi2-min ${PHI2MIN} --phi2-max ${PHI2MAX} --data-to-save ${DATA_TO_SAVE}
scrinet_gen_wf_data_7d_prec_single_spin_coprec --grid random --npts 10 -v --n-cores 4 --output-dir ${OUTPUT_DIR}/test_wf_data --q-min ${QMIN} --q-max ${QMAX} --chi1-min ${CHI1MIN} --chi1-max ${CHI1MAX} --theta1-min ${THETA1MIN} --theta1-max ${THETA1MAX} --phi1-min ${PHI1MIN} --phi1-max ${PHI1MAX} --chi2-min ${CHI2MIN} --chi2-max ${CHI2MAX} --theta2-min ${THETA2MIN} --theta2-max ${THETA2MAX} --phi2-min ${PHI2MIN} --phi2-max ${PHI2MAX} --data-to-save ${DATA_TO_SAVE}

# build reduced basis
scrinet_build_rb --data-to-model ${DATA1} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data
scrinet_build_rb --data-to-model ${DATA2} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data
scrinet_build_rb --data-to-model ${DATA3} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data
scrinet_build_rb --data-to-model ${DATA4} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data
scrinet_build_rb --data-to-model ${DATA5} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data


# training data to fit
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA1} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA2} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA3} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA4} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA5} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb

# validation data to test fit
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA1} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA2} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA3} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA4} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA5} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb

# fit
scrinet_fit --plot-fits --data-to-model ${DATA1} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

scrinet_fit --plot-fits --data-to-model ${DATA2} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

scrinet_fit --plot-fits --data-to-model ${DATA3} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

scrinet_fit --plot-fits --data-to-model ${DATA4} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

scrinet_fit --plot-fits --data-to-model ${DATA5} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

# evaluate

scrinet_evaluate_7d_coprec_model -v --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/train_wf_data \
--alpha-basis ${OUTPUT_DIR}/rb/alpha/alpha_eim_basis.npy --alpha-model-dir ${OUTPUT_DIR}/ts/alpha/fits \
--beta-basis ${OUTPUT_DIR}/rb/beta/beta_eim_basis.npy --beta-model-dir ${OUTPUT_DIR}/ts/beta/fits \
--gamma-basis ${OUTPUT_DIR}/rb/gamma/gamma_eim_basis.npy --gamma-model-dir ${OUTPUT_DIR}/ts/gamma/fits \
