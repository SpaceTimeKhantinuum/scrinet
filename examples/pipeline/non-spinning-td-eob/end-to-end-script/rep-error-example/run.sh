

BASISDIR=/Users/spx8sk/work/git/stk/scrinet/examples/pipeline/non-spinning-td-eob/end-to-end-script/one-dim-ouput/rb
TSDIR=/Users/spx8sk/work/git/stk/scrinet/examples/pipeline/non-spinning-td-eob/end-to-end-script/one-dim-ouput/ts
WFDIR=/Users/spx8sk/work/git/stk/scrinet/examples/pipeline/non-spinning-td-eob/end-to-end-script/one-dim-ouput

scrinet_representation_error \
    -v \
    --basis-model real-imag \
    --real-basis $BASISDIR/real/real_eim_basis.npy \
    --real-alpha $TSDIR/real/train/y.npy \
    --imag-basis $BASISDIR/imag/imag_eim_basis.npy \
    --imag-alpha $TSDIR/imag/train/y.npy \
    --wf-dir $WFDIR/train_wf_data \
    --num-per-chunk 100

scrinet_representation_error \
    -v \
    --basis-model real-imag \
    --real-basis $BASISDIR/real/real_eim_basis.npy \
    --real-alpha $TSDIR/real/val/y.npy \
    --imag-basis $BASISDIR/imag/imag_eim_basis.npy \
    --imag-alpha $TSDIR/imag/val/y.npy \
    --wf-dir $WFDIR/validation_wf_data \
    --num-per-chunk 100

