# PARAMS



# DATA_TO_SAVE="time amp phase freq real imag"
# DATA_TO_SAVE="time amp phase real imag"
DATA_TO_SAVE="time amp phase"
#DATA_TO_SAVE="time real imag"

DATA1=amp
DATA2=phase

#DATA1=real
#DATA2=imag

QMAX=10

OUTPUT_DIR=one-dim-ouput-amp-phase

BATCH_SIZE=10

INPUT_UNITS="128"
HIDDEN_UNITS="128 128 128 128 128"

# INPUT_UNITS="64"
# HIDDEN_UNITS="64 64 64 64 64 64 64 64 64 64 64 64 64"

# INPUT_UNITS="64"
# HIDDEN_UNITS="64 128 256 512 256 128 64"
LEARN_RATE=0.001
# LEARN_RATE=0.0001
#LEARN_RATE=0.000001
# LEARN_RATE=0.0005

# EPOCHS=2000
# EPOCHS=1000
EPOCHS=500
# EPOCHS=6000

LR_SCH=""
# LR_SCH="--lr-schedule"

USE_ALR="--use-alr"
ALR_VERBOSE=1
ALR_MIN_LR="1e-5"
ALR_PATIENCE=100
ALR_FACTOR=0.2

USE_ES="--use-es"
ES_VERBOSE=1
ES_PATIENCE=1000

KERNEL_INITIALIZER="he_uniform"
# KERNEL_INITIALIZER="he_normal"
# KERNEL_INITIALIZER="glorot_uniform"

#ACTIVATION='relu'
ACTIVATION='elu'
# ACTIVATION='tanh'

# gen wf data
scrinet_gen_wf_data_non_spinning --grid random --npts 1 -v --n-cores 1 --output-dir ${OUTPUT_DIR}/seed_wf_data --q-max ${QMAX} --data-to-save ${DATA_TO_SAVE}
scrinet_gen_wf_data_non_spinning --grid random --npts 1000 -v --n-cores 30 --output-dir ${OUTPUT_DIR}/train_wf_data --q-max ${QMAX} --data-to-save ${DATA_TO_SAVE}
scrinet_gen_wf_data_non_spinning --grid random --npts 1000 -v --n-cores 30 --output-dir ${OUTPUT_DIR}/validation_wf_data --q-max ${QMAX} --data-to-save ${DATA_TO_SAVE}
scrinet_gen_wf_data_non_spinning --grid random --npts 1000 -v --n-cores 30 --output-dir ${OUTPUT_DIR}/test_wf_data --q-max ${QMAX} --data-to-save ${DATA_TO_SAVE}

# build reduced basis
scrinet_build_rb --data-to-model ${DATA1} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data --use-rompy
scrinet_build_rb --data-to-model ${DATA2} -v --greedy-tol 1e-6 --output-dir ${OUTPUT_DIR}/rb --seed-dir ${OUTPUT_DIR}/seed_wf_data --train-dir ${OUTPUT_DIR}/train_wf_data --use-rompy


# training data to fit
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA1} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val train --data-to-model ${DATA2} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/train_wf_data --basis-dir ${OUTPUT_DIR}/rb

# validation data to test fit
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA1} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb
scrinet_gen_ts_data --train-or-val val --data-to-model ${DATA2} -v --basis-method eim --output-dir ${OUTPUT_DIR}/ts --wf-dir ${OUTPUT_DIR}/validation_wf_data --basis-dir ${OUTPUT_DIR}/rb

# fit
scrinet_fit --plot-fits --data-to-model ${DATA1} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}
scrinet_fit --plot-fits --data-to-model ${DATA2} -v --scaleX --scaleY --epochs ${EPOCHS}  --batch-size ${BATCH_SIZE} --ts-dir ${OUTPUT_DIR}/ts --output-dir ${OUTPUT_DIR}/ts --input-units ${INPUT_UNITS} --hidden-units ${HIDDEN_UNITS}  --learning-rate ${LEARN_RATE} ${LR_SCH} --kernel-initializer ${KERNEL_INITIALIZER} --activation ${ACTIVATION} ${USE_ALR} --alr-verbose ${ALR_VERBOSE} --alr-min-lr ${ALR_MIN_LR} --alr-patience ${ALR_PATIENCE} --alr-factor ${ALR_FACTOR} ${USE_ES} --es-verbose ${ES_VERBOSE} --es-patience ${ES_PATIENCE}

# evaluate

if [ $DATA1 == 'amp' ] && [ $DATA2 == 'phase' ]
then
    scrinet_evaluate_model -v --basis-model amp-phase --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy  --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/test_wf_data
    scrinet_evaluate_model -v --basis-model amp-phase --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy  --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/train_wf_data
    scrinet_evaluate_model -v --basis-model amp-phase --output-dir ${OUTPUT_DIR}/evaluate --amp-basis ${OUTPUT_DIR}/rb/amp/amp_eim_basis.npy  --amp-model-dir ${OUTPUT_DIR}/ts/amp/fits --phase-basis ${OUTPUT_DIR}/rb/phase/phase_eim_basis.npy  --phase-model-dir ${OUTPUT_DIR}/ts/phase/fits --wf-dir ${OUTPUT_DIR}/validation_wf_data
elif [ $DATA1 == 'real' ] && [ $DATA2 == 'imag' ]
then
    scrinet_evaluate_model -v --basis-model real-imag --output-dir ${OUTPUT_DIR}/evaluate --real-basis ${OUTPUT_DIR}/rb/real/real_eim_basis.npy  --real-model-dir ${OUTPUT_DIR}/ts/real/fits --imag-basis ${OUTPUT_DIR}/rb/imag/imag_eim_basis.npy  --imag-model-dir ${OUTPUT_DIR}/ts/imag/fits --wf-dir ${OUTPUT_DIR}/test_wf_data
    scrinet_evaluate_model -v --basis-model real-imag --output-dir ${OUTPUT_DIR}/evaluate --real-basis ${OUTPUT_DIR}/rb/real/real_eim_basis.npy  --real-model-dir ${OUTPUT_DIR}/ts/real/fits --imag-basis ${OUTPUT_DIR}/rb/imag/imag_eim_basis.npy  --imag-model-dir ${OUTPUT_DIR}/ts/imag/fits --wf-dir ${OUTPUT_DIR}/train_wf_data
    scrinet_evaluate_model -v --basis-model real-imag --output-dir ${OUTPUT_DIR}/evaluate --real-basis ${OUTPUT_DIR}/rb/real/real_eim_basis.npy  --real-model-dir ${OUTPUT_DIR}/ts/real/fits --imag-basis ${OUTPUT_DIR}/rb/imag/imag_eim_basis.npy  --imag-model-dir ${OUTPUT_DIR}/ts/imag/fits --wf-dir ${OUTPUT_DIR}/validation_wf_data
fi
