import sys
import time
import numpy as np
from schwimmbad import MPIPool
import schwimmbad
import lal
import lalsimulation as lalsim
import phenom
from scrinet.interfaces import lalutils

def worker(q):

    M = 60
    deltaT = 1/1024.
    f_min = 10
    inclination = 0.
    approximant = lalsim.SEOBNRv4_opt
    t_min = -1000
    t_max = 100

    m1, m2 = phenom.m1_m2_M_q(M, q)
    pp = dict(m1=m1, m2=m2, deltaT=deltaT,
            f_min=f_min,
            inclination=inclination,
            approximant=approximant
            )

    p = lalutils.gen_td_wf_params(**pp)
    t, amp, phase = lalutils.gen_td_wf(p, t_min=t_min, t_max=t_max)

    return t, amp, phase

def main(pool, mass_ratios):

    tasks = list(mass_ratios)

    results = pool.map(worker, tasks)

    return results


class SinglePool(object):
    """
    from pycbc/pool.py
    used for when n_cores = 1
    """
    def broadcast(self, fcn, args):
        return self.map(fcn, [args])

    def map(self, f, items):
        return [f(a) for a in items]

if __name__ == "__main__":

    # mpiexec -n n_cores python mpi-example.py



    with MPIPool() as pool:
        if not pool.is_master():
            pool.wait()
            sys.exit(0)

        # pool.size = n_cores - 1
        POOL_SIZE = pool.size
        print(f"POOL_SIZE = {POOL_SIZE}")

        mass_ratios = np.linspace(1, 10, 1000)

        t1 = time.time()
        results = main(pool, mass_ratios)
        print(f"time taken = {time.time() - t1}")

        # results is a list of len(mass_ratios)
        # results[0] are the times
        # results[1] are the amplitudes
        # results[2] are the phases

        print(len(results))
        print(len(results[0]))

        # import matplotlib.pyplot as plt
        # plt.figure()
        # for i in range(len(results)):
        #     plt.plot(results[i][0], results[i][1])
        # plt.show()